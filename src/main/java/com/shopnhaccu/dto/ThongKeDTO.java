package com.shopnhaccu.dto;

public class ThongKeDTO extends AbstractDTO<ThongKeDTO> {
	private int idproduct;
	private String product_name;
	private int product_amount;
	private float product_price;
	private float tongtien;
	public ThongKeDTO() {
		super();
	}
	public ThongKeDTO(int idproduct, String product_name, int product_amount, float product_price, float tongtien) {
		super();
		this.idproduct = idproduct;
		this.product_name = product_name;
		this.product_amount = product_amount;
		this.product_price = product_price;
		this.tongtien = tongtien;
	}
	public int getIdproduct() {
		return idproduct;
	}
	public void setIdproduct(int idproduct) {
		this.idproduct = idproduct;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public int getProduct_amount() {
		return product_amount;
	}
	public void setProduct_amount(int product_amount) {
		this.product_amount = product_amount;
	}
	public float getProduct_price() {
		return product_price;
	}
	public void setProduct_price(float product_price) {
		this.product_price = product_price;
	}
	public float getTongtien() {
		return tongtien;
	}
	public void setTongtien(float tongtien) {
		this.tongtien = tongtien;
	}
	
}
