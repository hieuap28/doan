package com.shopnhaccu.dto;

import java.util.List;

public class UserDTO extends AbstractDTO<UserDTO> {
	private int id;
	private String username;
	private String password;
	private String fullname;
	private int status;
	private String role_name;
	private List idrole;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getRole_name() {
		return role_name;
	}
	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}
	public List getIdrole() {
		return idrole;
	}
	public void setIdrole(List idrole) {
		this.idrole = idrole;
	}
	
	
	
}
