package com.shopnhaccu.dto;

public class ProductDTO extends AbstractDTO<ProductDTO> {
	
	private int id;
	private String product_description;
	private String product_image;
	private String product_name;
	private float product_price;
	private int product_amount;
	private int brand_id;
	private int category_id;
	private String category_name;
	private String brand_name;
	
	public ProductDTO(int id, String product_description, String product_image, String product_name,
			float product_price, int product_amount, int brand_id, int category_id) {
		super();
		this.id = id;
		this.product_description = product_description;
		this.product_image = product_image;
		this.product_name = product_name;
		this.product_price = product_price;
		this.product_amount = product_amount;
		this.brand_id = brand_id;
		this.category_id = category_id;
	}
	public ProductDTO() {
		super();
	}
	
	
	public ProductDTO(int id, String product_description, String product_image, String product_name,
			float product_price) {
		super();
		this.id = id;
		this.product_description = product_description;
		this.product_image = product_image;
		this.product_name = product_name;
		this.product_price = product_price;
	}
	public String getProduct_description() {
		return product_description;
	}
	public void setProduct_description(String product_description) {
		this.product_description = product_description;
	}
	public String getProduct_image() {
		return product_image;
	}
	public void setProduct_image(String product_image) {
		this.product_image = product_image;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public float getProduct_price() {
		return product_price;
	}
	public void setProduct_price(float product_price) {
		this.product_price = product_price;
	}
	public int getBrand_id() {
		return brand_id;
	}
	public void setBrand_id(int brand_id) {
		this.brand_id = brand_id;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public String getBrand_name() {
		return brand_name;
	}
	public void setBrand_name(String brand_name) {
		this.brand_name = brand_name;
	}
	public int getProduct_amount() {
		return product_amount;
	}
	public void setProduct_amount(int product_amount) {
		this.product_amount = product_amount;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
	
}
