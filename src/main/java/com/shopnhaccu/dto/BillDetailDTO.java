package com.shopnhaccu.dto;

public class BillDetailDTO extends AbstractDTO<BillDetailDTO> {

	private int idbill;
	private int idproduct;
	private String image_product;
	private String name_product;
	private int amount;
	private float price;
	public int getIdbill() {
		return idbill;
	}
	public void setIdbill(int idbill) {
		this.idbill = idbill;
	}
	public int getIdproduct() {
		return idproduct;
	}
	public void setIdproduct(int idproduct) {
		this.idproduct = idproduct;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
		
	public String getImage_product() {
		return image_product;
	}
	public void setImage_product(String image_product) {
		this.image_product = image_product;
	}
	public String getName_product() {
		return name_product;
	}
	public void setName_product(String name_product) {
		this.name_product = name_product;
	}
	public BillDetailDTO(int idbill, int idproduct,String image_product,String name_product, int amount, float price) {
		super();
		this.idbill = idbill;
		this.idproduct = idproduct;
		this.image_product = image_product;
		this.name_product = name_product;
		this.amount = amount;
		this.price = price;
	}
	public BillDetailDTO() {
		super();
	}
	
	
}
