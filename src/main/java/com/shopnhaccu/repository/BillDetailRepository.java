package com.shopnhaccu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shopnhaccu.entity.BillDetailEntity;

@Repository
public interface BillDetailRepository extends JpaRepository<BillDetailEntity, Integer> {
}
