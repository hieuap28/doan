package com.shopnhaccu.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.shopnhaccu.entity.ProductEntity;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Integer> {
	
	@Query(value = "SELECT p FROM ProductEntity p WHERE p.product_name LIKE '%' || :keyword || '%'")
	public List<ProductEntity> search(@Param("keyword") String keyword);
}
