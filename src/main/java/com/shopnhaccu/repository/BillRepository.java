package com.shopnhaccu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shopnhaccu.entity.BillEntity;

@Repository
public interface BillRepository extends JpaRepository<BillEntity, Integer> {
}
