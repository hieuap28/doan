package com.shopnhaccu.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.shopnhaccu.dto.UserDTO;
import com.shopnhaccu.service.IUserService;

@Controller
public class UserController {

	@Autowired
	private IUserService userService;

	@RequestMapping("/quan-tri/taikhoan/danhsach")
	public ModelAndView index(@RequestParam("page") int page, @RequestParam("limit") int limit,
			HttpServletRequest request) {
		UserDTO model = new UserDTO();
		model.setPage(page);
		model.setLimit(limit);
		ModelAndView mav = new ModelAndView("admin/user-page");
		model.setListResult(userService.findAll(new PageRequest(page - 1, limit)));
		model.setTotalItem(userService.getTotalItem());
		model.setTotalPage((int) Math.ceil((double) model.getTotalItem() / model.getLimit()));
		mav.addObject("model", model);
		return mav;
	}

	@RequestMapping("/quan-tri/init-user-insert")
	public String initinsert(Model m) {
		UserDTO u = new UserDTO();
		m.addAttribute("u", u);
		return "admin/user-insert";
	}
	
	@RequestMapping("/quan-tri/user-insert")
	public String insert(@ModelAttribute("u") UserDTO u) {

		
		return "admin/user-insert";
	}
}
