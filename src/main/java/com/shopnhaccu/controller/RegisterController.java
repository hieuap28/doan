package com.shopnhaccu.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.shopnhaccu.entity.UserEntity;
import com.shopnhaccu.service.IUserService;

@Controller
public class RegisterController {

	@Autowired
	IUserService userService;

	@RequestMapping("/dang-ky")
	public String registerPage(Model m) {
		return "register";
	}

	@RequestMapping("/dang-ky/user-insert")
	public String insert(@ModelAttribute("u") UserEntity u, Model m,HttpServletRequest request, HttpServletResponse response) {
		boolean success = userService.insert(u);
		if (success) {
			m.addAttribute("msg", "Thêm thành công");
			return "register";
		} else {
			m.addAttribute("msg", "Thất bại");
			return "register";
		}

	}

}
