package com.shopnhaccu.controller;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.shopnhaccu.dto.BrandDTO;
import com.shopnhaccu.dto.CategoryDTO;
import com.shopnhaccu.dto.NewDTO;
import com.shopnhaccu.dto.ProductDTO;
import com.shopnhaccu.service.INewService;
import com.shopnhaccu.util.MessageUtil;

@Controller
public class NewController {

	@Autowired
	private INewService newService;
	
	@Autowired
	private MessageUtil messageUtil;
	
	@RequestMapping("/quan-tri/baiviet/danhsach")
	public ModelAndView loginPage(@RequestParam("page") int page,@RequestParam("limit") int limit,HttpServletRequest request) {
		NewDTO model = new NewDTO();
		model.setPage(page);
		model.setLimit(limit);
		ModelAndView mav = new ModelAndView("admin/new-page");
		Pageable pageable = new PageRequest(page-1, limit);
		model.setListResult(newService.findAll(pageable));
		model.setTotalItem(newService.getTotalItem());
		model.setTotalPage((int) Math.ceil((double) model.getTotalItem() / model.getLimit()));
		if(request.getParameter("message") != null) {
			Map<String,String> message = messageUtil.getMessage(request.getParameter("message"));
			mav.addObject("message",message.get("message") );
		}
		mav.addObject("model",model);
		
		return mav;
	   } 
	
	@RequestMapping("/quan-tri/init-new-insert")
	public String initinsert(Model m) {
		NewDTO n = new NewDTO();
		
		
		m.addAttribute("n",n);
		return "admin/new-insert";
	}
	
	@RequestMapping("/quan-tri/new-insert")
	public String insertWithImage(@ModelAttribute("n") NewDTO n, HttpServletRequest request, Model m) {
		try {
			ServletFileUpload sfu = new ServletFileUpload(new DiskFileItemFactory());
			List<FileItem> lstItem = sfu.parseRequest(request);
			for (FileItem item : lstItem) {
				if (item.isFormField()) {
					try {
						String data = item.getString("UTF-8");
						switch (item.getFieldName()) {
						case "title":
							n.setTitle(data);
							break;
						case "shortdescription":
							n.setShortdescription(data);
							break;
						case "content":
							n.setContent(data);
							break;
						}
					} catch (UnsupportedEncodingException ex) {
						Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
					}
				} else {
					String filepath = request.getServletContext().getRealPath("template/image");
					String filename = item.getName();
					File destination = new File(filepath, filename);
					try {
						if (destination.exists()) {
							destination.delete();
						}
						item.write(destination);
						n.setImage(filename); // Tên file ảnh
						
					} catch (Exception ex) {
						Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}

			
			boolean success = newService.insert(n);
			if (success) {
				return "redirect:baiviet/danhsach?page=1&limit=2"; 

			} else {
				m.addAttribute("msg", "Lỗi thêm dữ liệu");
				return "admin/new-insert";
			}
		} catch (FileUploadException ex) {
			Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
			m.addAttribute("msg", "Lỗi thêm dữ liệu ảnh");
			return "admin/new-insert";
		}
	}
	
	@RequestMapping("/quan-tri/init-new-update")
	public String initupdate(Model m,@RequestParam("id") Integer id) {
		NewDTO n = new NewDTO();
		n = newService.findById(id);
		m.addAttribute("n", n);
		return "admin/new-update";
	}
	
	@RequestMapping("/quan-tri/new-update")
    public String updatetWithImage(@ModelAttribute("n") NewDTO n, HttpServletRequest request, Model m) {
        try {
            ServletFileUpload sfu = new ServletFileUpload(new DiskFileItemFactory());
            List<FileItem> lstItem = sfu.parseRequest(request);
            String imagelinkold = "";
            for (FileItem item : lstItem) {
                if (item.isFormField()) {
                    try {
                        String data = item.getString("UTF-8");
                        switch (item.getFieldName()) {
                            case "id":
                                n.setId(Integer.parseInt(data));
                                break;
                            case "title":
    							n.setTitle(data);
    							break;
    						case "shortdescription":
    							n.setShortdescription(data);
    							break;
    						case "content":
    							n.setContent(data);
    							break;
                            case "imagelinkold":
                                n.setImage(data);
                                //imagelinkold=data;
                                break;
                        }
                    } catch (UnsupportedEncodingException ex) {
                        Logger.getLogger(NewController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    String filepath = request.getServletContext().getRealPath("template/image");
                    String filename = item.getName();
                    if (filename == null || filename.isEmpty()) {
                        //System.out.println("linh áº£nh cÅ©" + imagelinkold);
                    } else if (filename != null || !filename.isEmpty()) {
                        File destination = new File(filepath, filename);
                        try {
                            if (destination.exists()) {
                                destination.delete();
                            }
                            item.write(destination);
                            n.setImage(filename);
                        } catch (Exception ex) {
                            Logger.getLogger(NewController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                }
            }

            m.addAttribute("n", n);

            

            boolean success = newService.update(n);
            //boolean success = false;
            if (success) {
                return "redirect:baiviet/danhsach?page=1&limit=2";
            } else {
                m.addAttribute("msg", "Lỗi thêm dữ liệu");
                return "admin/new-update";
            }
        } catch (FileUploadException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            m.addAttribute("msg", "Lỗi thêm dữ liệu ảnh");
            return "admin/new-update";
        }
    }
	
	@RequestMapping("/quan-tri/new-delete")
    public String delete(@RequestParam("id") int id, Model m) {
        
        boolean success = newService.delete(id);
        return "redirect:baiviet/danhsach?page=1&limit=2"; 
    }

}
