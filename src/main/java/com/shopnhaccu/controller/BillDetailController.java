package com.shopnhaccu.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.shopnhaccu.dto.BillDTO;
import com.shopnhaccu.dto.BillDetailDTO;
import com.shopnhaccu.service.IBillDetailService;
import com.shopnhaccu.service.IBillService;
import com.shopnhaccu.util.MessageUtil;

@Controller
public class BillDetailController {

	@Autowired
	private IBillDetailService billDetailService;

	@Autowired
	private MessageUtil messageUtil;
	
	@RequestMapping("/quan-tri/billdetail-delete")
    public String delete(@RequestParam("id") int id, Model m) {
        
        boolean success = billDetailService.delete(id);
        return "redirect:cthoadon/danhsach"; 
    }
}
