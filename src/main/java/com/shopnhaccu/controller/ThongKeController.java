package com.shopnhaccu.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.shopnhaccu.dto.ThongKeDTO;
import com.shopnhaccu.service.IProductService;

@Controller
public class ThongKeController {
	@Autowired
	private IProductService productService;
	
	@RequestMapping("/quan-tri/thongke/danhsach")
	public String index(Model m) {
		
//		List<ThongKeDTO> list = productService.thongke(thang, nam);
//		m.addAttribute("list", list);
		return "admin/thongke";
	}
	
	@RequestMapping("/quan-tri/thongke/sanpham")
	public String thongke(Model m,@RequestParam("thang") int thang,@RequestParam("nam") int nam) {
		List<ThongKeDTO> list = productService.thongke(thang, nam);
		m.addAttribute("list", list);
		return "admin/thongke";
	}
	
}
