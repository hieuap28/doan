package com.shopnhaccu.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.shopnhaccu.dto.CategoryDTO;
import com.shopnhaccu.service.ICategoryService;
import com.shopnhaccu.util.MessageUtil;

@Controller
public class CategoryController {

	@Autowired
	private ICategoryService categoryService;

	@Autowired
	private MessageUtil messageUtil;

	@RequestMapping("/quan-tri/loaisanpham/danhsach")
	public ModelAndView loginPage(HttpServletRequest request) {
		CategoryDTO model = new CategoryDTO();
		ModelAndView mav = new ModelAndView("admin/category-page");
		model.setListResult(categoryService.findAll());
		if (request.getParameter("message") != null) {
			Map<String, String> message = messageUtil.getMessage(request.getParameter("message"));
			mav.addObject("message", message.get("message"));
			mav.addObject("alert", message.get("alert"));
		}
		mav.addObject("model", model);
		return mav;
	}

	@RequestMapping("/quan-tri/init-category-insert")
	public String initinsert(Model m) {
		CategoryDTO c = new CategoryDTO();

		m.addAttribute("c", c);
		return "admin/category-insert";
	}

	@RequestMapping("/quan-tri/category-insert")
	public String insert(@ModelAttribute("c") CategoryDTO c,Model m) {

		boolean success = categoryService.insert(c);
		if (success) {
//			String msg = "Thêm mới thành công";
			m.addAttribute("success",success);
			m.addAttribute("msg","Thêm mới thành công");
			return "admin/category-insert";
		}else {
			m.addAttribute("success",success);
			m.addAttribute("msg","Thêm mới thất bại");
			return "admin/category-insert";
		}

	}

	@RequestMapping("/quan-tri/init-category-update")
	public String initupdate(Model m, @RequestParam("id") Integer id) {
		CategoryDTO c = new CategoryDTO();
		c = categoryService.findById(id);
		m.addAttribute("c", c);
		return "admin/category-update";
	}

	@RequestMapping("/quan-tri/category-update")
	public String update(@ModelAttribute("c") CategoryDTO c) {
		boolean success = categoryService.update(c);
		return "redirect:loaisanpham/danhsach";
	}

	@RequestMapping("/quan-tri/category-delete")
	public String delete(@RequestParam("id") int id, Model m) {

		boolean success = categoryService.delete(id);
		return "redirect:loaisanpham/danhsach";
	}
}
