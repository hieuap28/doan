package com.shopnhaccu.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.shopnhaccu.dto.BillDTO;
import com.shopnhaccu.dto.BillDetailDTO;
import com.shopnhaccu.dto.BrandDTO;
import com.shopnhaccu.dto.CategoryDTO;
import com.shopnhaccu.dto.NewDTO;
import com.shopnhaccu.dto.ProductDTO;
import com.shopnhaccu.entity.ProductEntity;
import com.shopnhaccu.service.IBillDetailService;
import com.shopnhaccu.service.IBillService;
import com.shopnhaccu.service.IBrandService;
import com.shopnhaccu.service.ICategoryService;
import com.shopnhaccu.service.INewService;
import com.shopnhaccu.service.IProductService;

@Controller
public class FrontendController {

	@Autowired
	private IProductService productService;

	@Autowired
	private ICategoryService categoryService;

	@Autowired
	private IBrandService brandService;

	@Autowired
	private IBillService billService;

	@Autowired
	private IBillDetailService billDetailService;
	
	@Autowired
	private INewService newService;

	@RequestMapping("/home")
	public String homePage(Model m, Integer page) {
		int itemsInPage = 30;
		int total = productService.getTotalItem();
		int pages = (int) Math.ceil((float) total / (float) itemsInPage);
		List<Integer> lstPages = new ArrayList<Integer>();
		for (int i = 0; i < pages; i++) {
			lstPages.add(i);
		}
		if (page == null) {
			page = new Integer(0);
		}


		List<CategoryDTO> listc = categoryService.findAll();
		List<BrandDTO> listb = brandService.findAll();
		List<ProductDTO> list = productService.findProducthot();
		List<ProductDTO> listh = productService.findProductnew();
		m.addAttribute("listh", listh);
		m.addAttribute("list", list);
		m.addAttribute("listc", listc);
		m.addAttribute("listb", listb);
		m.addAttribute("lstPages", lstPages);
		m.addAttribute("listgt", productService.getProductbycategory(1));
		m.addAttribute("listpiano", productService.getProductbycategory(2));
		m.addAttribute("listog", productService.getProductbycategory(3));
		m.addAttribute("listt", productService.getProductbycategory(4));
		m.addAttribute("listk", productService.getProductbycategory(5));
		return "web/trang-chu";
	}

	@RequestMapping("/chitietsanpham")
	public String detail(@RequestParam("id") Integer id, Model m) {
		List<CategoryDTO> listc = categoryService.findAll();
		ProductDTO p = productService.findById(id);
		List<BrandDTO> listb = brandService.findAll();
		List<ProductDTO> list = productService.findProducthot();
		List<ProductDTO> listh = productService.findProductnew();
		m.addAttribute("list", list);
		m.addAttribute("listh", listh);
		m.addAttribute("listc", listc);
		m.addAttribute("listb", listb);
		m.addAttribute("p", p);
		return "web/chitietsanpham";
	}

	@RequestMapping("/add-cart")
	public String add2cart(@RequestParam Integer idp, HttpServletRequest request, Model m) {
		// Add giỏ hàng vào session
		HashMap<Integer, Integer> lstCart = (HashMap<Integer, Integer>) request.getSession().getAttribute("cart");
		if (lstCart == null) {
			lstCart = new HashMap<>();

		}
		if (lstCart.get(idp) == null) {
			lstCart.put(idp, 1);
		} else {
			int soluong = lstCart.get(idp);
			lstCart.put(idp, soluong + 1);
		}
		request.getSession().setAttribute("cart", lstCart);
		return "redirect:home";
	}

	@RequestMapping("/update-cart")
	public String update(HttpServletRequest request, Model m) {
		HashMap<Integer, Integer> lstCart = (HashMap<Integer, Integer>) request.getSession().getAttribute("cart");
		String [] product_amount= request.getParameterValues("product_amount");
		int i=-1;
		for (Map.Entry<Integer, Integer> set : lstCart.entrySet()) {
			i++;
			ProductDTO p = new ProductDTO();
			p = productService.findById(set.getKey());
			p.setProduct_amount(set.getValue());
			lstCart.put(set.getKey(),Integer.parseInt(product_amount[i]) );
		}
		
		return "redirect:view-cart";
	}
	
	@RequestMapping("/card-delete")
	public String delete(@RequestParam Integer idp, HttpServletRequest request, Model m) {
		HashMap<Integer, Integer> lstCart = (HashMap<Integer, Integer>) request.getSession().getAttribute("cart");
		lstCart.remove(idp);
		return "redirect:view-cart";

	}

	@RequestMapping("/category-detail")
	public String detail(@RequestParam("id") Integer id, Model m, Integer page) {
		CategoryDTO c = categoryService.findById(id);
		m.addAttribute("c", c);
		List<CategoryDTO> listc = categoryService.findAll();
		List<BrandDTO> listb = brandService.findAll();
		List<ProductDTO> listp = productService.getByCategory(id);
		List<ProductDTO> list = productService.findProducthot();
		List<ProductDTO> listh = productService.findProductnew();
		m.addAttribute("list", list);
		m.addAttribute("listh", listh);
		m.addAttribute("listp", listp);
		m.addAttribute("listc", listc);
		m.addAttribute("listb", listb);
		return "web/category_detail";
	}

	@RequestMapping("/brand-detail")
	public String detail1(@RequestParam("id") Integer id, @RequestParam("idb") Integer idb, Model m, Integer page) {
		CategoryDTO c = categoryService.findById(id);
		BrandDTO b = brandService.findById(idb);
		m.addAttribute("b", b);
		m.addAttribute("c", c);
		List<CategoryDTO> listc = categoryService.findAll();
		List<BrandDTO> listb = brandService.findAll();
		List<ProductDTO> listp = productService.getByBrand(id, idb);
		List<ProductDTO> list = productService.findProducthot();
		List<ProductDTO> listh = productService.findProductnew();
		m.addAttribute("list", list);
		m.addAttribute("listh", listh);
		m.addAttribute("listp", listp);
		m.addAttribute("listc", listc);
		m.addAttribute("listb", listb);
		return "web/brand_detail";
	}

	@RequestMapping("/view-cart")
	public String viewCart(Model m, HttpServletRequest request) {
		HashMap<Integer, Integer> lstCart = (HashMap<Integer, Integer>) request.getSession().getAttribute("cart");
		List<ProductDTO> listp = new ArrayList<>();
		List<CategoryDTO> listc = categoryService.findAll();
		List<BrandDTO> listb = brandService.findAll();
		List<ProductDTO> list = productService.findProducthot();
		List<ProductDTO> listh = productService.findProductnew();
		int tongtien = 0;
		for (Map.Entry<Integer, Integer> set : lstCart.entrySet()) {
			ProductDTO p = new ProductDTO();
			p = productService.findById(set.getKey());
			p.setProduct_amount(set.getValue());
			tongtien += p.getProduct_amount() * p.getProduct_price();
			listp.add(p);
		}

		m.addAttribute("tongtien", tongtien);
		m.addAttribute("lstCart", lstCart);
		m.addAttribute("listp", listp);
		m.addAttribute("listc", listc);
		m.addAttribute("listb", listb);
		m.addAttribute("list", list);
		m.addAttribute("listh", listh);

		return "web/view-cart";
	}

	@RequestMapping("/init-thanhtoan")
	public String thanhtoan(Model m, HttpServletRequest request, @ModelAttribute("bill") BillDTO bill,
			BindingResult result) {
		HashMap<Integer, Integer> lstCart = (HashMap<Integer, Integer>) request.getSession().getAttribute("cart");
		List<CategoryDTO> listc = categoryService.findAll();
		List<BrandDTO> listb = brandService.findAll();
		int tongtien = 0;
		for (Map.Entry<Integer, Integer> set : lstCart.entrySet()) {
			ProductDTO p = new ProductDTO();
			p = productService.findById(set.getKey());
			p.setProduct_amount(set.getValue());
			tongtien += p.getProduct_amount() * p.getProduct_price();
		}
		List<ProductDTO> list = productService.findProducthot();
		List<ProductDTO> listh = productService.findProductnew();
		m.addAttribute("list", list);
		m.addAttribute("listh", listh);
		m.addAttribute("tongtien", tongtien);
		m.addAttribute("bill", bill);
		m.addAttribute("listc", listc);
		m.addAttribute("listb", listb);
		return "web/thanhtoan"; // Tải dữ liệu lên trang index.jsp
	}

	@RequestMapping("/thanhtoan")
	public String thanhtoan1(Model m, HttpServletRequest request, @ModelAttribute("bill") BillDTO bill,
			BindingResult result) {
		try {
			HashMap<Integer, Integer> lstCart = (HashMap<Integer, Integer>) request.getSession().getAttribute("cart");
			List<CategoryDTO> listc = categoryService.findAll();
			List<BrandDTO> listb = brandService.findAll();
			int tongtien = 0;
			BillDTO bil = billService.insert(bill);
			for (Map.Entry<Integer, Integer> set : lstCart.entrySet()) {
				ProductDTO p = new ProductDTO();
				p = productService.findById(set.getKey());
				p.setProduct_amount(set.getValue());
				billDetailService.insert(new BillDetailDTO(bil.getId(), p.getId(),p.getProduct_image(),p.getProduct_name(), p.getProduct_amount(), p.getProduct_price()));			
			}
			m.addAttribute("tongtien", tongtien);
			m.addAttribute("listc", listc);
			m.addAttribute("listb", listb);
			return "web/thongbao"; // Tải dữ liệu lên trang index.jsp
		} catch (Exception e) {
			return "web/thanhtoan";
		}

	}

	@RequestMapping("/thongbao")
	public String thongbao(Model m) {
		List<CategoryDTO> listc = categoryService.findAll();
		List<BrandDTO> listb = brandService.findAll();
		List<ProductDTO> list = productService.findProducthot();
		List<ProductDTO> listh = productService.findProductnew();
		m.addAttribute("list", list);
		m.addAttribute("listh", listh);
		m.addAttribute("listc", listc);
		m.addAttribute("listb", listb);
		return "web/thongbao"; // Tải dữ liệu lên trang index.jsp
	}

	@RequestMapping("/gioithieu")
	public String gioithieu(Model m) {
		List<CategoryDTO> listc = categoryService.findAll();
		List<BrandDTO> listb = brandService.findAll();
		List<ProductDTO> list = productService.findProducthot();
		List<ProductDTO> listh = productService.findProductnew();
		m.addAttribute("list", list);
		m.addAttribute("listh", listh);
		m.addAttribute("listc", listc);
		m.addAttribute("listb", listb);

		return "web/gioithieu"; // Tải dữ liệu lên trang index.jsp
	}

	@RequestMapping("/lienhe")
	public String lienhe(Model m) {
		List<CategoryDTO> listc = categoryService.findAll();
		List<BrandDTO> listb = brandService.findAll();
		List<ProductDTO> list = productService.findProducthot();
		List<ProductDTO> listh = productService.findProductnew();
		m.addAttribute("list", list);
		m.addAttribute("listh", listh);
		m.addAttribute("listc", listc);
		m.addAttribute("listb", listb);

		return "web/lienhe"; // Tải dữ liệu lên trang index.jsp
	}
	
	@RequestMapping("/baiviet")
	public String baiviet(Model m) {
		List<CategoryDTO> listc = categoryService.findAll();
		List<BrandDTO> listb = brandService.findAll();
		List<NewDTO> listn = newService.findAll();
		List<ProductDTO> list = productService.findProducthot();
		List<ProductDTO> listh = productService.findProductnew();
		m.addAttribute("list", list);
		m.addAttribute("listh", listh);
		m.addAttribute("listc", listc);
		m.addAttribute("listb", listb);
		m.addAttribute("listn", listn);

		return "web/baiviet"; // Tải dữ liệu lên trang index.jsp
	}
	
	@RequestMapping("/new-detail")
	public String newdetail(@RequestParam("id") Integer id, Model m) {
		NewDTO n = newService.findById(id);
		m.addAttribute("n", n);
		List<CategoryDTO> listc = categoryService.findAll();
		List<BrandDTO> listb = brandService.findAll();
		List<ProductDTO> list = productService.findProducthot();
		List<ProductDTO> listh = productService.findProductnew();
		m.addAttribute("list", list);
		m.addAttribute("listh", listh);
		m.addAttribute("listc", listc);
		m.addAttribute("listb", listb);
		return "web/new_detail";
	}
	
	@RequestMapping("/search")
	public String search(@RequestParam("keyword") String keyword,Model m) {
		List<ProductEntity> result = productService.search(keyword);
		List<CategoryDTO> listc = categoryService.findAll();
		List<BrandDTO> listb = brandService.findAll();
		List<ProductDTO> list = productService.findProducthot();
		List<ProductDTO> listh = productService.findProductnew();
		m.addAttribute("list", list);
		m.addAttribute("listh", listh);
		m.addAttribute("listc", listc);
		m.addAttribute("listb", listb);
		m.addAttribute("result",result);
		return "web/timkiem";
	}

}
