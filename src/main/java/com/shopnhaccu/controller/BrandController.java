package com.shopnhaccu.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.shopnhaccu.dto.BrandDTO;
import com.shopnhaccu.service.IBrandService;
import com.shopnhaccu.util.MessageUtil;

@Controller
public class BrandController {

	@Autowired
	private IBrandService brandService;

	@Autowired
	private MessageUtil messageUtil;

	@RequestMapping("/quan-tri/hangsanpham/danhsach")
	public ModelAndView loginPage(HttpServletRequest request) {
		BrandDTO model = new BrandDTO();
		ModelAndView mav = new ModelAndView("admin/brand-page");
		model.setListResult(brandService.findAll());
		if (request.getParameter("message") != null) {
			Map<String, String> message = messageUtil.getMessage(request.getParameter("message"));
			mav.addObject("message", message.get("message"));
		}
		mav.addObject("model", model);
		return mav;
	}

	@RequestMapping("/quan-tri/init-brand-insert")
	public String initinsert(Model m) {
		BrandDTO b = new BrandDTO();
		m.addAttribute("b", b);
		return "admin/brand-insert";
	}

	@RequestMapping("/quan-tri/brand-insert")
	public String insert(@ModelAttribute("b") BrandDTO b) {

		boolean success = brandService.insert(b);
		return "admin/brand-insert";
	}
	
	@RequestMapping("/quan-tri/init-brand-update")
	public String initupdate(Model m,@RequestParam("id") Integer id) {
		BrandDTO b = new BrandDTO();
		b = brandService.findById(id);
		m.addAttribute("b", b);
		return "admin/brand-update";
	}
	
	@RequestMapping("/quan-tri/brand-update")
	public String update(@ModelAttribute("b") BrandDTO b) {

		boolean success = brandService.update(b);
		return "redirect:hangsanpham/danhsach";
	}
	
	@RequestMapping("/quan-tri/brand-delete")
    public String delete(@RequestParam("id") int id, Model m) {
        
        boolean success = brandService.delete(id);
        return "redirect:hangsanpham/danhsach"; 
    }
}
