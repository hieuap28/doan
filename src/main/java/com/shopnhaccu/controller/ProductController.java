package com.shopnhaccu.controller;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.shopnhaccu.dto.BrandDTO;
import com.shopnhaccu.dto.CategoryDTO;
import com.shopnhaccu.dto.ProductDTO;
import com.shopnhaccu.service.IBrandService;
import com.shopnhaccu.service.ICategoryService;
import com.shopnhaccu.service.IProductService;
import com.shopnhaccu.util.MessageUtil;

@Controller
public class ProductController {

	@Autowired
	private IProductService productService;

	@Autowired
	private ICategoryService categoryService;

	@Autowired
	private IBrandService brandService;

	@Autowired
	private MessageUtil messageUtil;

	@RequestMapping("/quan-tri/sanpham/danhsach")
	public ModelAndView index(@RequestParam("page") int page, @RequestParam("limit") int limit,
			HttpServletRequest request) {
		ProductDTO model = new ProductDTO();
		model.setPage(page);
		model.setLimit(limit);
		ModelAndView mav = new ModelAndView("admin/product-page");
//		Pageable pageable = new PageRequest(page - 1, limit);
		model.setListResult(productService.findAll(new PageRequest(page - 1, limit)));
		model.setTotalItem(productService.getTotalItem());
		model.setTotalPage((int) Math.ceil((double) model.getTotalItem() / model.getLimit()));
		if (request.getParameter("message") != null) {
			Map<String, String> message = messageUtil.getMessage(request.getParameter("message"));
			mav.addObject("message", message.get("message"));
		}
		mav.addObject("model", model);
		return mav;
	}

	@RequestMapping("/quan-tri/init-product-insert")
	public String initinsert(Model m) {
		ProductDTO p = new ProductDTO();
		List<CategoryDTO> listc = categoryService.findAll();
		List<BrandDTO> listb = brandService.findAll();
		m.addAttribute("p", p);
		m.addAttribute("listc", listc);
		m.addAttribute("listb", listb);
		return "admin/product-insert";
	}

	@RequestMapping("/quan-tri/product-insert")
	public String insertWithImage(@ModelAttribute("p") ProductDTO p, HttpServletRequest request, Model m) {
		try {
			ServletFileUpload sfu = new ServletFileUpload(new DiskFileItemFactory());
			List<FileItem> lstItem = sfu.parseRequest(request);
			for (FileItem item : lstItem) {
				if (item.isFormField()) {
					try {
						String data = item.getString("UTF-8");
						switch (item.getFieldName()) {
						case "product_name":
							p.setProduct_name(data);
							break;
						case "product_price":
							p.setProduct_price(Float.parseFloat(data));
							break;
						case "product_amount":
							p.setProduct_amount(Integer.parseInt(data));
							break;
						case "category_id":
							p.setCategory_id(Integer.parseInt(data));
							break;
						case "brand_id":
							p.setBrand_id(Integer.parseInt(data));
							break;
						case "product_description":
							p.setProduct_description(data);
							break;
						}
					} catch (UnsupportedEncodingException ex) {
						Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
					}
				} else {
					String filepath = request.getServletContext().getRealPath("template/image");
					String filename = item.getName();
					File destination = new File(filepath, filename);
					try {
						if (destination.exists()) {
							destination.delete();
						}
						item.write(destination);
						p.setProduct_image(filename); // Tên file ảnh
					} catch (Exception ex) {
						Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}

			
			boolean success = productService.insert(p);
			if (success) {
				return "redirect:sanpham/danhsach?page=1&limit=4"; // Ä?iá»?u hÆ°á»›ng vá»? trang chÃ­nh

			} else {
				m.addAttribute("msg", "Lỗi thêm dữ liệu");
				return "admin/product-insert";
			}
		} catch (FileUploadException ex) {
			Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
			m.addAttribute("msg", "Lỗi thêm dữ liệu ảnh");
			return "admin/product-insert";
		}
	}

	@RequestMapping("/quan-tri/init-product-update")
	public String initupdate(Model m,@RequestParam("id") Integer id) {
		ProductDTO p = new ProductDTO();
		p = productService.findById(id);
		List<CategoryDTO> listc = categoryService.findAll();
		List<BrandDTO> listb = brandService.findAll();
		m.addAttribute("p", p);
		m.addAttribute("listc", listc);
		m.addAttribute("listb", listb);
		return "admin/product-update";
	}
	
	@RequestMapping("/quan-tri/product-update")
    public String updatetWithImage(@ModelAttribute("p") ProductDTO p, HttpServletRequest request, Model m) {
        try {
            ServletFileUpload sfu = new ServletFileUpload(new DiskFileItemFactory());
            List<FileItem> lstItem = sfu.parseRequest(request);
            String imagelinkold = "";
            for (FileItem item : lstItem) {
                if (item.isFormField()) {
                    try {
                        String data = item.getString("UTF-8");
                        switch (item.getFieldName()) {
                            case "id":
                                p.setId(Integer.parseInt(data));
                                break;
                            case "product_name":
    							p.setProduct_name(data);
    							break;
    						case "product_price":
    							p.setProduct_price(Float.parseFloat(data));
    							break;
    						case "product_amount":
    							p.setProduct_amount(Integer.parseInt(data));
    							break;
    						case "category_id":
    							p.setCategory_id(Integer.parseInt(data));
    							break;
    						case "brand_id":
    							p.setBrand_id(Integer.parseInt(data));
    							break;
    						case "product_description":
    							p.setProduct_description(data);
    							break;
                            case "imagelinkold":
                                p.setProduct_image(data);
                                //imagelinkold=data;
                                break;
                        }
                    } catch (UnsupportedEncodingException ex) {
                        Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    String filepath = request.getServletContext().getRealPath("template/image");
                    String filename = item.getName();
                    if (filename == null || filename.isEmpty()) {
                        //System.out.println("linh áº£nh cÅ©" + imagelinkold);
                    } else if (filename != null || !filename.isEmpty()) {
                        File destination = new File(filepath, filename);
                        try {
                            if (destination.exists()) {
                                destination.delete();
                            }
                            item.write(destination);
                            p.setProduct_image(filename);
                        } catch (Exception ex) {
                            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                }
            }

            m.addAttribute("p", p);

            

            boolean success = productService.update(p);
            //boolean success = false;
            if (success) {
                return "redirect:sanpham/danhsach?page=1&limit=4";
            } else {
                m.addAttribute("msg", "Lỗi thêm dữ liệu");
                return "admin/product-update";
            }
        } catch (FileUploadException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            m.addAttribute("msg", "Lỗi thêm dữ liệu ảnh");
            return "admin/product-update";
        }
    }
	
	@RequestMapping("/quan-tri/product-delete")
    public String delete(@RequestParam("id") int id, Model m) {
        
        boolean success = productService.delete(id);
        return "redirect:sanpham/danhsach?page=1&limit=4"; 
    }
}
