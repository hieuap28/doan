package com.shopnhaccu.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.shopnhaccu.dto.BillDTO;
import com.shopnhaccu.dto.BillDetailDTO;
import com.shopnhaccu.dto.BrandDTO;
import com.shopnhaccu.dto.CategoryDTO;
import com.shopnhaccu.dto.ProductDTO;
import com.shopnhaccu.service.IBillDetailService;
import com.shopnhaccu.service.IBillService;
import com.shopnhaccu.util.MessageUtil;

@Controller
public class BillController {

	@Autowired
	private IBillService billService;
	
	@Autowired
	private IBillDetailService billDetailService;

	@Autowired
	private MessageUtil messageUtil;

	@RequestMapping("/quan-tri/hoadon/danhsach")
	public ModelAndView loginPage(HttpServletRequest request) {
		BillDTO model = new BillDTO();
		ModelAndView mav = new ModelAndView("admin/bill-page");
		model.setListResult(billService.findAll());
		if (request.getParameter("message") != null) {
			Map<String, String> message = messageUtil.getMessage(request.getParameter("message"));
			mav.addObject("message", message.get("message"));
		}
		mav.addObject("model", model);

		return mav;
	}

	@RequestMapping("/quan-tri/bill-detail")
	public String detail(@RequestParam("id") Integer id, Model m) {
		BillDTO b = billService.findById(id);
		List<BillDetailDTO> list = billDetailService.getByBill(id);
		m.addAttribute("b", b);
		m.addAttribute("list", list);
		return "admin/billdetail-page";
	}
	
	@RequestMapping("/quan-tri/init-bill-update")
	public String initupdate(Model m,@RequestParam("id") Integer id) {
		BillDTO b = new BillDTO();
		b = billService.findById(id);
		m.addAttribute("b", b);
		return "admin/bill-update";
	}
	
	@RequestMapping("/quan-tri/bill-update")
	public String update(@ModelAttribute("b") BillDTO b) {

		boolean success = billService.update(b);
		return "redirect:hoadon/danhsach";
	}
		
	@RequestMapping("/quan-tri/bill-delete")
    public String delete(@RequestParam("id") int id, Model m) {
        
        boolean success = billService.delete(id);
        return "redirect:hoadon/danhsach"; 
    }
}
