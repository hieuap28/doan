package com.shopnhaccu.service;

import java.util.List;

import com.shopnhaccu.dto.CategoryDTO;

public interface ICategoryService {

	public List<CategoryDTO> findAll();
	CategoryDTO findById(Integer id);
	boolean insert(CategoryDTO c);
	boolean update(CategoryDTO p);
	boolean delete(Integer id);
}
