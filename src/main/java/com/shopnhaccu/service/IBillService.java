package com.shopnhaccu.service;

import java.util.List;

import com.shopnhaccu.dto.BillDTO;

public interface IBillService {

	public List<BillDTO> findAll();
	BillDTO findById(Integer id);
	BillDTO insert(BillDTO bill);
	boolean update(BillDTO bill);
	boolean delete(Integer id);
}
