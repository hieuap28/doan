package com.shopnhaccu.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.shopnhaccu.dto.ProductDTO;
import com.shopnhaccu.dto.ThongKeDTO;
import com.shopnhaccu.entity.ProductEntity;

public interface IProductService {

	List<ProductDTO> findAll(Pageable pageble);
	List<ProductDTO> findProducthot();
	List<ProductDTO> findProductnew();
	List<ProductDTO> getProductbycategory(Integer idloai);
	List<ProductDTO> getByCategory(Integer categoryid);
	List<ProductDTO> getByBrand(Integer categoryid,Integer brandid);
	int getTotalItem();
	ProductDTO findById(Integer id);
	boolean insert(ProductDTO p);
	boolean update(ProductDTO p);
	boolean delete(Integer id);
	List<ProductEntity> search(String keyword);
	List<ThongKeDTO> thongke(int thang,int nam);
}
