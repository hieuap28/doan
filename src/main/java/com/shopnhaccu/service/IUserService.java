package com.shopnhaccu.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.shopnhaccu.dto.UserDTO;
import com.shopnhaccu.entity.UserEntity;

public interface IUserService {
	List<UserDTO> findAll(Pageable pageble);
	boolean insert(UserEntity u);
	int getTotalItem();

}
