package com.shopnhaccu.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shopnhaccu.controller.CategoryController;
import com.shopnhaccu.converter.CategoryConverter;
import com.shopnhaccu.dto.CategoryDTO;
import com.shopnhaccu.entity.CategoryEntity;
import com.shopnhaccu.repository.CategoryRepository;
import com.shopnhaccu.service.ICategoryService;

@Service
public class CategoryService implements ICategoryService {

	@Autowired
	private CategoryRepository categoryRepository;
		
	@Autowired
	private CategoryConverter categoryConverter;
	
	@Override
	public List<CategoryDTO> findAll() {
		List<CategoryDTO> models = new ArrayList<>();
		List<CategoryEntity> entities = categoryRepository.findAll();
		for (CategoryEntity item: entities) {
			CategoryDTO categoryDTO = categoryConverter.toDto(item);
			 models.add(categoryDTO);			
		}
		return models;
	}

	@Override
	@Transactional
	public boolean insert(CategoryDTO c) {
		try {
			CategoryEntity categoryEntity = new CategoryEntity();
			categoryEntity = categoryConverter.toEntity(c);
			categoryConverter.toDto(categoryRepository.save(categoryEntity));
			return true;
			
		}catch(Exception e) {
			System.out.println(e.getMessage());
			return false;
		}	
		
	}

	@Override
	public CategoryDTO findById(Integer id) {
		CategoryEntity entity = categoryRepository.findOne(id);
		return categoryConverter.toDto(entity);
	}

	@Override
	@Transactional
	public boolean update(CategoryDTO c) {
		try {
			CategoryEntity oldCategory = categoryRepository.findOne(c.getId());
			CategoryEntity updateCategory = categoryConverter.toEntity(oldCategory, c);
			categoryConverter.toDto(categoryRepository.save(updateCategory));
			return true;
		}catch(Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
		
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		try {
			categoryRepository.delete(id);
			return true;
        } catch (Exception ex) {
            Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
		
	}	
}
