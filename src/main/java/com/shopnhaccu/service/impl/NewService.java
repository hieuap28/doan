package com.shopnhaccu.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shopnhaccu.controller.NewController;
import com.shopnhaccu.controller.ProductController;
import com.shopnhaccu.converter.NewConverter;
import com.shopnhaccu.dto.NewDTO;
import com.shopnhaccu.entity.NewEntity;
import com.shopnhaccu.repository.NewRepository;
import com.shopnhaccu.service.INewService;

@Service
public class NewService implements INewService {

	@Autowired
	private NewRepository newRepository;

	@Autowired
	private NewConverter newConverter;

	@Override
	public List<NewDTO> findAll(Pageable pageble) {
		List<NewDTO> models = new ArrayList<>();
		List<NewEntity> entities = newRepository.findAll(pageble).getContent();
		for (NewEntity item : entities) {
			NewDTO newDTO = newConverter.toDto(item);
			models.add(newDTO);
		}
		return models;
	}

	@Override
	public List<NewDTO> findAll() {
		List<NewDTO> models = new ArrayList<>();
		List<NewEntity> entities = newRepository.findAll();
		for (NewEntity item : entities) {
			NewDTO newDTO = newConverter.toDto(item);
			models.add(newDTO);
		}
		return models;
	}

	@Override
	public int getTotalItem() {

		return (int) newRepository.count();
	}

	@Override
	public NewDTO findById(Integer id) {
		NewEntity entity = newRepository.findOne(id);
		return newConverter.toDto(entity);
	}

	@Override
	@Transactional
	public boolean insert(NewDTO n) {
		try {
			NewEntity newEntity = new NewEntity();
			newEntity = newConverter.toEntity(n);
			newConverter.toDto(newRepository.save(newEntity));
			return true;
		} catch (Exception e) {
			Logger.getLogger(NewController.class.getName()).log(Level.SEVERE, null, e);
			return false;
		}

	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		try {
			newRepository.delete(id);
			return true;
		} catch (Exception ex) {
			Logger.getLogger(NewController.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}

	}

	@Override
	@Transactional
	public boolean update(NewDTO n) {
		try {
			NewEntity oldNew = newRepository.findOne(n.getId());
			NewEntity updateNew = newConverter.toEntity(oldNew, n);
			newConverter.toDto(newRepository.save(updateNew));
			return true;
		} catch (Exception ex) {
			Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}

	}
}
