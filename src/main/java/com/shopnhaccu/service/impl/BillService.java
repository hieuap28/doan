package com.shopnhaccu.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shopnhaccu.controller.CategoryController;
import com.shopnhaccu.converter.BillConverter;
import com.shopnhaccu.dto.BillDTO;
import com.shopnhaccu.entity.BillEntity;
import com.shopnhaccu.repository.BillRepository;
import com.shopnhaccu.service.IBillService;

@Service
public class BillService implements IBillService {

	@Autowired
	private BillRepository billRepository;
		
	@Autowired
	private BillConverter billConverter;
	
	@Override
	public List<BillDTO> findAll() {
		List<BillDTO> models = new ArrayList<>();
		List<BillEntity> entities = billRepository.findAll();
		for (BillEntity item: entities) {
			BillDTO billDTO = billConverter.toDto(item);
			 models.add(billDTO);			
		}
		return models;
	}

	@Override
	@Transactional
	public BillDTO insert(BillDTO bill) {
			BillEntity billEntity = new BillEntity();
			billEntity = billConverter.toEntity(bill);
			billConverter.toDto(billRepository.save(billEntity));
			return billConverter.toDto(billRepository.save(billEntity));
	}

	@Override
	public BillDTO findById(Integer id) {
		BillEntity entity = billRepository.findOne(id);
		return billConverter.toDto(entity);
	}

	@Override
	@Transactional
	public boolean update(BillDTO bill) {
		try {
			BillEntity oldCategory = billRepository.findOne(bill.getId());
			BillEntity updateCategory = billConverter.toEntity(oldCategory, bill);
			billConverter.toDto(billRepository.save(updateCategory));
			return true;
		}catch(Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
		
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		try {
			billRepository.delete(id);
			return true;
        } catch (Exception ex) {
            Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
		
	}	
}
