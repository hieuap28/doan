package com.shopnhaccu.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopnhaccu.converter.BrandConverter;
import com.shopnhaccu.dto.BrandDTO;
import com.shopnhaccu.entity.BrandEntity;
import com.shopnhaccu.repository.BrandRepository;
import com.shopnhaccu.service.IBrandService;

@Service
public class BrandService implements IBrandService {

	@Autowired
	private BrandRepository brandRepository;
		
	@Autowired
	private BrandConverter brandConverter;
	
	@Override
	public List<BrandDTO> findAll() {
		List<BrandDTO> models = new ArrayList<>();
		List<BrandEntity> entities = brandRepository.findAll();
		for (BrandEntity item: entities) {
			BrandDTO brandDTO = brandConverter.toDto(item);
			 models.add(brandDTO);			
		}
		return models;
	}

	@Override
	public int getTotalItem() {
		return (int) brandRepository.count();
	}

	@Override
	public BrandDTO findById(int id) {
		BrandEntity entity = brandRepository.findOne(id);
		return brandConverter.toDto(entity);
	}

	@Override
	public boolean insert(BrandDTO b) {
		try {
			BrandEntity brandEntity = new BrandEntity();
			brandEntity = brandConverter.toEntity(b);
			brandConverter.toDto(brandRepository.save(brandEntity));
			return true;
			
		}catch(Exception e) {
			System.out.println(e.getMessage());
			return false;
		}	
	}

	@Override
	public boolean update(BrandDTO b) {
		try {
			BrandEntity oldBrand = brandRepository.findOne(b.getId());
			BrandEntity updateBrand = brandConverter.toEntity(oldBrand, b);
			brandConverter.toDto(brandRepository.save(updateBrand));
			return true;
		}catch(Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

	@Override
	public boolean delete(Integer id) {
		try{
			brandRepository.delete(id);
			return true;
		}catch(Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

}
