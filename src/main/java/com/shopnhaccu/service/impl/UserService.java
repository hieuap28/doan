package com.shopnhaccu.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shopnhaccu.converter.UserConverter;
import com.shopnhaccu.dto.UserDTO;
import com.shopnhaccu.entity.RoleEntity;
import com.shopnhaccu.entity.UserEntity;
import com.shopnhaccu.repository.RoleRepository;
import com.shopnhaccu.repository.UserRepository;
import com.shopnhaccu.service.IUserService;

@Service
public class UserService implements IUserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserConverter userConverter;

	@Autowired
	private RoleRepository roleRepository;

	@Override
	@Transactional
	public boolean insert(UserEntity u) {
		try {
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			String encodedPassword = encoder.encode(u.getPassword());
			RoleEntity role = roleRepository.findOne(2);
			u.setPassword(encodedPassword);
			u.getRoles().add(role);
			userRepository.save(u);
			return true;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}

	}

	@Override
	public List<UserDTO> findAll(Pageable pageble) {
		List<UserDTO> models = new ArrayList<>();
		List<UserEntity> entities = userRepository.findAll(pageble).getContent();
		for (UserEntity item : entities) {
			UserDTO userDTO = userConverter.toDto(item);
			models.add(userDTO);
		}
		return models;
	}
	
	@Override
	public int getTotalItem() {
		return (int) userRepository.count();
	}
}
