package com.shopnhaccu.service.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.CallableStatement;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import com.shopnhaccu.controller.CategoryController;
import com.shopnhaccu.controller.NewController;
import com.shopnhaccu.controller.ProductController;
import com.shopnhaccu.converter.ProductConverter;
import com.shopnhaccu.dto.ProductDTO;
import com.shopnhaccu.dto.ThongKeDTO;
import com.shopnhaccu.entity.BrandEntity;
import com.shopnhaccu.entity.CategoryEntity;
import com.shopnhaccu.entity.ProductEntity;
import com.shopnhaccu.repository.BrandRepository;
import com.shopnhaccu.repository.CategoryRepository;
import com.shopnhaccu.repository.ProductRepository;
import com.shopnhaccu.service.IProductService;
import com.shopnhaccu.util.MyConnector;

@Service
public class ProductService implements IProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private BrandRepository brandRepository;

	@Autowired
	private ProductConverter productConverter;

	@Override
	public List<ProductDTO> findAll(Pageable pageble) {
		List<ProductDTO> models = new ArrayList<>();
		List<ProductEntity> entities = productRepository.findAll(pageble).getContent();
		for (ProductEntity item : entities) {
			ProductDTO productDTO = productConverter.toDto(item);
			models.add(productDTO);
		}
		return models;
	}

	@Override
	public int getTotalItem() {
		return (int) productRepository.count();
	}

	@Override
	public ProductDTO findById(Integer id) {
		ProductEntity entity = productRepository.findOne(id);
		return productConverter.toDto(entity);
	}

	@Override
	@Transactional
	public boolean insert(ProductDTO p) {
		try {
			CategoryEntity category = categoryRepository.findOne(p.getCategory_id());
			BrandEntity brand = brandRepository.findOne(p.getBrand_id());
			ProductEntity productEntity = new ProductEntity();
			productEntity = productConverter.toEntity(p);
			productEntity.setCategory(category);
			productEntity.setBrand(brand);
			productConverter.toDto(productRepository.save(productEntity));
			return true;
		} catch (Exception e) {
			Logger.getLogger(NewController.class.getName()).log(Level.SEVERE, null, e);
			return false;
		}

	}

	@Override
	@Transactional
	public boolean update(ProductDTO p) {
		try {
			ProductEntity oldProduct = productRepository.findOne(p.getId());
			CategoryEntity category = categoryRepository.findOne(p.getCategory_id());
			BrandEntity brand = brandRepository.findOne(p.getBrand_id());
			oldProduct.setCategory(category);
			oldProduct.setBrand(brand);
			ProductEntity updateproduct = productConverter.toEntity(oldProduct, p);
			productConverter.toDto(productRepository.save(updateproduct));
			return true;
		} catch (Exception ex) {
			Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}

	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		try {
			productRepository.delete(id);
			return true;
		} catch (Exception ex) {
			Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}

	}

	@Override
	public List<ProductDTO> getByCategory(Integer categoryid) {
		Connection cnn = MyConnector.getConnection();
		try {
			String sql = "SELECT * FROM `product` WHERE `category_id` = ?";
			PreparedStatement pstt = (PreparedStatement) cnn.prepareStatement(sql);
			pstt.setInt(1, categoryid);
			ResultSet rs = pstt.executeQuery();
			List<ProductDTO> list = new ArrayList<ProductDTO>();
			while (rs.next()) {
				int id = rs.getInt("id");
				Integer product_amount = rs.getInt("product_amount");
				String product_description = rs.getString("product_description");
				String product_name = rs.getString("product_name");
				Float product_price = rs.getFloat("product_price");
				Integer brand_id = rs.getInt("brand_id");
				Integer category_id = rs.getInt("category_id");
				String product_image = rs.getString("product_image");
				ProductDTO p = new ProductDTO(id, product_description, product_image, product_name, product_price,
						product_amount, brand_id, category_id);
				list.add(p);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Lỗi: " + e.getMessage());
			return null;
		}

	}

	@Override
	public List<ProductDTO> getByBrand(Integer categoryid, Integer brandid) {
		Connection cnn = MyConnector.getConnection();
		try {
			String sql = "SELECT * FROM `product` WHERE `category_id` = ? and `brand_id`=?";
			PreparedStatement pstt = (PreparedStatement) cnn.prepareStatement(sql);
			pstt.setInt(1, categoryid);
			pstt.setInt(2, brandid);
			ResultSet rs = pstt.executeQuery();
			List<ProductDTO> list = new ArrayList<ProductDTO>();
			while (rs.next()) {
				int id = rs.getInt("id");
				Integer product_amount = rs.getInt("product_amount");
				String product_description = rs.getString("product_description");
				String product_name = rs.getString("product_name");
				Float product_price = rs.getFloat("product_price");
				Integer brand_id = rs.getInt("brand_id");
				Integer category_id = rs.getInt("category_id");
				String product_image = rs.getString("product_image");
				ProductDTO p = new ProductDTO(id, product_description, product_image, product_name, product_price,
						product_amount, brand_id, category_id);
				list.add(p);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Lỗi: " + e.getMessage());
			return null;
		}
	}

	@Override
	public List<ProductDTO> findProducthot() {
		try {
			String sql = "{CALL sp_producthot1()}";
			Statement stt = (Statement) MyConnector.getConnection().createStatement();
			ResultSet rs = stt.executeQuery(sql);
			List<ProductDTO> list = new ArrayList<>();
			while (rs.next()) {
				int id = rs.getInt("id");
				String product_name = rs.getString("product_name");
				String product_image = rs.getString("product_image");
				String product_description = rs.getString("product_description");
				Float product_price = rs.getFloat("product_price");
				ProductDTO p = new ProductDTO(id, product_description, product_image, product_name, product_price);
				list.add(p);
			}
			return list;
		} catch (SQLException ex) {
			ex.printStackTrace();
			System.out.println("Lỗi: " + ex.getMessage());
			return null;
		}

	}

	@Override
	public List<ProductDTO> findProductnew() {
		try {
			String sql = "{CALL sp_spnew()}";
			Statement stt = (Statement) MyConnector.getConnection().createStatement();
			ResultSet rs = stt.executeQuery(sql);
			List<ProductDTO> list = new ArrayList<>();
			while (rs.next()) {
				int id = rs.getInt("id");
				String product_name = rs.getString("product_name");
				String product_image = rs.getString("product_image");
				String product_description = rs.getString("product_description");
				Float product_price = rs.getFloat("product_price");
				ProductDTO p = new ProductDTO(id, product_description, product_image, product_name, product_price);
				list.add(p);
			}
			return list;
		} catch (SQLException ex) {
			ex.printStackTrace();
			System.out.println("Lỗi: " + ex.getMessage());
			return null;
		}
	}

	@Override
	public List<ProductDTO> getProductbycategory(Integer idloai) {
		try {
			Connection cnn = MyConnector.getConnection();
			String sql="{CALL sp_spbyloai(?)}";
			CallableStatement cstt = (CallableStatement) cnn.prepareCall(sql);
			cstt.setInt(1, idloai);
			ResultSet rs = cstt.executeQuery();
			List<ProductDTO> list = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String product_name = rs.getString("product_name");
				String product_image = rs.getString("product_image");
				String product_description = rs.getString("product_description");
				Float product_price = rs.getFloat("product_price");
				ProductDTO p = new ProductDTO(id, product_description, product_image, product_name, product_price);
				list.add(p);
            }
			return list;
		} catch (SQLException ex) {
			ex.printStackTrace();
			System.out.println("Lỗi: " + ex.getMessage());
			return null;
		}
	}

	@Override
	public List<ProductEntity> search(String keyword) {
		
		return productRepository.search(keyword);
	}

	@Override
	public List<ThongKeDTO> thongke(int thang, int nam) {
		try {
			Connection cnn = MyConnector.getConnection();
			String sql="{CALL sp_thongke(?,?)}";
			CallableStatement cstt = (CallableStatement) cnn.prepareCall(sql);
			cstt.setInt(1, thang);
			cstt.setInt(2, nam);
			ResultSet rs = cstt.executeQuery();
			List<ThongKeDTO> list = new ArrayList<>();
            while (rs.next()) {
                int idproduct = rs.getInt("idproduct");
                String product_name = rs.getString("product_name");
                int product_amount = rs.getInt("soluong");
				Float product_price = rs.getFloat("product_price");
				Float tongtien = rs.getFloat("tongtien");
				ThongKeDTO t = new ThongKeDTO(idproduct, product_name, product_amount, product_price, tongtien);
				list.add(t);
            }
			return list;
		} catch (SQLException ex) {
			ex.printStackTrace();
			System.out.println("Lỗi: " + ex.getMessage());
			return null;
		}
	}

}
