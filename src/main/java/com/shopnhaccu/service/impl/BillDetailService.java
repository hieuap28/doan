package com.shopnhaccu.service.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.PreparedStatement;
import com.shopnhaccu.controller.CategoryController;
import com.shopnhaccu.converter.BillDetailConverter;
import com.shopnhaccu.dto.BillDetailDTO;
import com.shopnhaccu.dto.ProductDTO;
import com.shopnhaccu.entity.BillDetailEntity;
import com.shopnhaccu.repository.BillDetailRepository;
import com.shopnhaccu.service.IBillDetailService;
import com.shopnhaccu.util.MyConnector;

@Service
public class BillDetailService implements IBillDetailService {

	@Autowired
	private BillDetailRepository billDetailRepository;

	@Autowired
	private BillDetailConverter billDetailConverter;

	@Override
	public List<BillDetailDTO> findAll() {
		List<BillDetailDTO> models = new ArrayList<>();
		List<BillDetailEntity> entities = billDetailRepository.findAll();
		for (BillDetailEntity item : entities) {
			BillDetailDTO billDTO = billDetailConverter.toDto(item);
			models.add(billDTO);
		}
		return models;
	}

	@Override
	@Transactional
	public boolean insert(BillDetailDTO bd) {
		try {
			String sql = "INSERT INTO `bill_detail`(`idbill`, `idproduct`,`image_product`,`name_product`, `amount`, `price`) VALUES (?,?,?,?,?,?)";
			PreparedStatement pstt = (PreparedStatement) MyConnector.getConnection().prepareCall(sql);
			pstt.setInt(1, bd.getIdbill());
			pstt.setInt(2, bd.getIdproduct());
			pstt.setString(3, bd.getImage_product());
			pstt.setString(4, bd.getName_product());
			pstt.setInt(5, bd.getAmount());
			pstt.setFloat(6, bd.getPrice());

			int row = pstt.executeUpdate();
			return (row > 0) ? true : false;
		} catch (SQLException ex) {
			Logger.getLogger(BillDetailDTO.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}
	}

	@Override
	public BillDetailDTO findById(Integer id) {
		BillDetailEntity entity = billDetailRepository.findOne(id);
		return billDetailConverter.toDto(entity);
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		try {
			billDetailRepository.delete(id);
			return true;
		} catch (Exception ex) {
			Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}

	}

	@Override
	public List<BillDetailDTO> getByBill(Integer billid) {
		Connection cnn = MyConnector.getConnection();
		try {
			String sql = "SELECT * FROM `bill_detail` WHERE `idbill` = ?";
			PreparedStatement pstt = (PreparedStatement) cnn.prepareStatement(sql);
			pstt.setInt(1, billid);
			ResultSet rs = pstt.executeQuery();
			List<BillDetailDTO> list = new ArrayList<BillDetailDTO>();
			while (rs.next()) {
				int idbill = rs.getInt("idbill");
				int idproduct = rs.getInt("idproduct");
				String image_product = rs.getString("image_product");
				String name_product = rs.getString("name_product");
				int amount = rs.getInt("amount");
				float price = rs.getFloat("price");
				BillDetailDTO b = new BillDetailDTO(idbill, idproduct, image_product, name_product, amount, price);
				list.add(b);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Lỗi: " + e.getMessage());
			return null;
		}
	}
}
