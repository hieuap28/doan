package com.shopnhaccu.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.shopnhaccu.dto.NewDTO;

public interface INewService {
	
	public List<NewDTO> findAll(Pageable pageble);
	public List<NewDTO> findAll();
	int getTotalItem();
	NewDTO findById(Integer id);
	boolean insert(NewDTO n);
	boolean update(NewDTO n);
	boolean delete(Integer id);
}
