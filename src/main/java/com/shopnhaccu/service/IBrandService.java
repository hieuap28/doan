package com.shopnhaccu.service;

import java.util.List;

import com.shopnhaccu.dto.BrandDTO;

public interface IBrandService {

	public List<BrandDTO> findAll();
	int getTotalItem();
	BrandDTO findById(int id);
	boolean insert(BrandDTO b);
	boolean update(BrandDTO b);
	boolean delete(Integer id);
}
