package com.shopnhaccu.service;

import java.util.List;

import com.shopnhaccu.dto.BillDetailDTO;
import com.shopnhaccu.dto.ProductDTO;

public interface IBillDetailService {

	public List<BillDetailDTO> findAll();
	BillDetailDTO findById(Integer id);
	List<BillDetailDTO> getByBill(Integer billid);
	boolean insert(BillDetailDTO bd);
	boolean delete(Integer id);
}
