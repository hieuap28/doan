package com.shopnhaccu.converter;

import org.springframework.stereotype.Component;

import com.shopnhaccu.dto.BrandDTO;
import com.shopnhaccu.entity.BrandEntity;

@Component
public class BrandConverter {

	public BrandDTO toDto(BrandEntity entity) {
		BrandDTO result = new BrandDTO();
		result.setId(entity.getId());
		result.setName(entity.getName());
		result.setCreatedBy(entity.getCreatedby());
		result.setCreatedDate( entity.getCreateddate());
		result.setModifiedBy(entity.getModifiedby());
		result.setModifiedDate( entity.getModifieddate());
		return result;
	}
	
	public BrandEntity toEntity(BrandDTO dto) {
		BrandEntity result = new BrandEntity();
		result.setName(dto.getName());
		return result;
	}
	
	public BrandEntity toEntity(BrandEntity result,BrandDTO dto) {
		result.setName(dto.getName());
		return result;
	}
}
