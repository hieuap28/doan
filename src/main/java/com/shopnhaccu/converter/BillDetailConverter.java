package com.shopnhaccu.converter;

import org.springframework.stereotype.Component;

import com.shopnhaccu.dto.BillDetailDTO;
import com.shopnhaccu.entity.BillDetailEntity;

@Component
public class BillDetailConverter {

	public BillDetailDTO toDto(BillDetailEntity entity) {
		BillDetailDTO result = new BillDetailDTO();
		result.setIdbill(entity.getIdbill());
		result.setIdproduct(entity.getIdproduct());
		result.setImage_product(entity.getImage_product());
		result.setName_product(entity.getName_product());
		result.setAmount(entity.getAmount());
		result.setPrice(entity.getPrice());
		return result;
	}
	
	public BillDetailEntity toEntity(BillDetailDTO dto) {
		BillDetailEntity result = new BillDetailEntity();
		result.setIdbill(dto.getIdbill());
		result.setIdproduct(dto.getIdproduct());
		result.setAmount(dto.getAmount());
		result.setPrice(dto.getPrice());
		return result;
	}
	
	public BillDetailEntity toEntity(BillDetailEntity result,BillDetailDTO dto) {
		result.setIdbill(dto.getIdbill());
		result.setIdproduct(dto.getIdproduct());
		result.setAmount(dto.getAmount());
		result.setPrice(dto.getPrice());
		return result;
	}
}
