package com.shopnhaccu.converter;

import org.springframework.stereotype.Component;

import com.shopnhaccu.dto.NewDTO;
import com.shopnhaccu.entity.NewEntity;

@Component
public class NewConverter {

	public NewDTO toDto(NewEntity entity) {
		NewDTO result = new NewDTO();
		result.setId(entity.getId());
		result.setTitle(entity.getTitle());
		result.setShortdescription(entity.getShortdescription());
		result.setContent(entity.getContent());
		result.setImage(entity.getImage());
		result.setCreatedDate(entity.getCreateddate());
		return result;
	}
	
	public NewEntity toEntity(NewDTO dto) {
		NewEntity result = new NewEntity();
		result.setTitle(dto.getTitle());
		result.setShortdescription(dto.getShortdescription());
		result.setContent(dto.getContent());
		result.setImage(dto.getImage());
		return result;
	}
	
	public NewEntity toEntity(NewEntity result,NewDTO dto) {
		result.setTitle(dto.getTitle());
		result.setShortdescription(dto.getShortdescription());
		result.setContent(dto.getContent());
		result.setImage(dto.getImage());
		return result;
	}
	
}
