package com.shopnhaccu.converter;

import org.springframework.stereotype.Component;

import com.shopnhaccu.dto.UserDTO;
import com.shopnhaccu.entity.RoleEntity;
import com.shopnhaccu.entity.UserEntity;

@Component
public class UserConverter {

	public UserDTO toDto(UserEntity entity) {
		UserDTO result = new UserDTO();
		result.setId(entity.getId());
		result.setFullname(entity.getFullName());
		result.setUsername(entity.getUserName());
		result.setPassword(entity.getPassword());
		result.setStatus(entity.getStatus());
		result.setRole_name(entity.getRoles().get(0).getName());
		return result;
	}
	
	public UserEntity toEntity(UserDTO dto) {
		UserEntity result = new UserEntity();
		result.setFullName(dto.getFullname());
		result.setUserName(dto.getUsername());
		result.setPassword(dto.getPassword());
		result.setStatus(dto.getStatus());
		return result;
	}
	
	public UserEntity toEntity(UserEntity result,UserDTO dto) {
		result.setFullName(dto.getFullname());
		result.setUserName(dto.getUsername());
		result.setPassword(dto.getPassword());
		result.setStatus(dto.getStatus());
		return result;
	}
	
}
