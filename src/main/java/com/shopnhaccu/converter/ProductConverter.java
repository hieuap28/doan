package com.shopnhaccu.converter;

import org.springframework.stereotype.Component;

import com.shopnhaccu.dto.ProductDTO;
import com.shopnhaccu.entity.ProductEntity;

@Component
public class ProductConverter {

	public ProductDTO toDto(ProductEntity entity) {
		ProductDTO result = new ProductDTO();
		result.setId(entity.getId());
		result.setProduct_description(entity.getProduct_description());
		result.setProduct_image(entity.getProduct_image());
		result.setProduct_name(entity.getProduct_name());
		result.setProduct_price(entity.getProduct_price());
		result.setProduct_amount(entity.getProduct_amount());
		result.setCategory_name(entity.getCategory().getName());
		result.setBrand_name(entity.getBrand().getName());
		result.setCategory_id(entity.getCategory().getId());
		return result;
	}
	
	public ProductEntity toEntity(ProductDTO p) {
		ProductEntity result = new ProductEntity();
		result.setProduct_name(p.getProduct_name());
		result.setProduct_description(p.getProduct_description());
		result.setProduct_amount(p.getProduct_amount());
		result.setProduct_price(p.getProduct_price());
		result.setProduct_image(p.getProduct_image());
		
		return result;
	}
	
	public ProductEntity toEntity(ProductEntity result,ProductDTO dto) {
		result.setProduct_name(dto.getProduct_name());
		result.setProduct_image(dto.getProduct_image());
		result.setProduct_price(dto.getProduct_price());
		result.setProduct_description(dto.getProduct_description());
		result.setProduct_amount(dto.getProduct_amount());
		return result;
	}
}
