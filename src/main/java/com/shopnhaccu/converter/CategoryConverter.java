package com.shopnhaccu.converter;

import org.springframework.stereotype.Component;

import com.shopnhaccu.dto.CategoryDTO;
import com.shopnhaccu.entity.CategoryEntity;

@Component
public class CategoryConverter {

	public CategoryDTO toDto(CategoryEntity entity) {
		CategoryDTO result = new CategoryDTO();
		result.setId(entity.getId());
		result.setName(entity.getName());
		result.setCreatedBy(entity.getCreatedby());
		result.setCreatedDate( entity.getCreateddate());
		result.setModifiedBy(entity.getModifiedby());
		result.setModifiedDate( entity.getModifieddate());
		return result;
	}
	
	public CategoryEntity toEntity(CategoryDTO dto) {
		CategoryEntity result = new CategoryEntity();
		result.setName(dto.getName());
		return result;
	}
	
	public CategoryEntity toEntity(CategoryEntity result,CategoryDTO dto) {
		result.setName(dto.getName());
		return result;
	}
}
