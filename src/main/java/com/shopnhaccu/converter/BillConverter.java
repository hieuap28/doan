package com.shopnhaccu.converter;

import org.springframework.stereotype.Component;

import com.shopnhaccu.dto.BillDTO;
import com.shopnhaccu.entity.BillEntity;

@Component
public class BillConverter {

	public BillDTO toDto(BillEntity entity) {
		BillDTO result = new BillDTO();
		result.setId(entity.getId());
		result.setTotalmoney(entity.getTotalmoney());
		result.setReceiver(entity.getReceiver());
		result.setAddress(entity.getAddress());
		result.setPhone(entity.getPhone());
		result.setEmail(entity.getEmail());
		result.setCreatedBy(entity.getCreatedby());
		result.setCreatedDate( entity.getCreateddate());
		result.setModifiedBy(entity.getModifiedby());
		result.setModifiedDate( entity.getModifieddate());
		result.setStatus(entity.getStatus());
		return result;
	}
	
	public BillEntity toEntity(BillDTO dto) {
		BillEntity result = new BillEntity();
		result.setTotalmoney(dto.getTotalmoney());
		result.setReceiver(dto.getReceiver());
		result.setAddress(dto.getAddress());
		result.setPhone(dto.getPhone());
		result.setEmail(dto.getEmail());
		result.setStatus(dto.getStatus());
		return result;
	}
	
	public BillEntity toEntity(BillEntity result,BillDTO dto) {
		result.setTotalmoney(dto.getTotalmoney());
		result.setReceiver(dto.getReceiver());
		result.setAddress(dto.getAddress());
		result.setPhone(dto.getPhone());
		result.setEmail(dto.getEmail());
		result.setStatus(dto.getStatus());
		return result;
	}
}
