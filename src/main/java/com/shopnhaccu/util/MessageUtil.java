package com.shopnhaccu.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class MessageUtil {

	public Map<String, String> getMessage(String message) {
		Map<String, String> result = new HashMap<>();

		if (message.equals("update_success")) {
			result.put("message", "Cập nhật thành công");

		} else if (message.equals("insert_success")) {
			result.put("message", "Thêm mới thành công");
		} else if (message.equals("error_system")) {
			result.put("message", "Lỗi");
		}

		return result;
	}
}
