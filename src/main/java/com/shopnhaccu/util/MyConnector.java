package com.shopnhaccu.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class MyConnector {

	private static final String URL = "jdbc:mysql://localhost:3306/shopnhaccu?useUnicode=true&characterEncoding=utf-8&useSSL=false";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "";
	
	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection cnn = DriverManager.getConnection(URL,USERNAME, PASSWORD);
			return cnn;	
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println("Lỗi:" + e.getMessage());
		}
		return null;
	}
}
