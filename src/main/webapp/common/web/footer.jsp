<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>

<div class="container-pluid">
	<section id="footer">
		<div class="container">
			<div class="col-md-3" id="shareicon">
				<ul>
					<li><a href='<c:url value="https://www.facebook.com/Shop-Nh%E1%BA%A1c-C%E1%BB%A5-106621294907900"></c:url>'><i class="fa fa-facebook"></i></a></li>
					<li><a href=""><i class="fa fa-twitter"></i></a></li>
					<li><a href=""><i class="fa fa-google-plus"></i></a></li>
					<li><a href=""><i class="fa fa-youtube"></i></a></li>
				</ul>
			</div>
			<div class="col-md-8" id="title-block">
				<div class="pull-left"></div>
				<div class="pull-right"></div>
			</div>

		</div>
	</section>
	<section id="footer-button">
		<div class="container-pluid">
			<div class="container">
				<div class="col-md-6" id="ft-about">
					<iframe
						src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FShop-Nh%25E1%25BA%25A1c-C%25E1%25BB%25A5-106621294907900&tabs=272&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=459702324713329"
						width="340" height="130" style="border: none; overflow: hidden"
						scrolling="no" frameborder="0" allowfullscreen="true"
						allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
				</div>

				<div class="col-md-3 box-footer">
					<h3 class="tittle-footer">thông tin</h3>
					<ul>
						<li><i class="fa fa-angle-double-right"></i> <a href='<c:url value="/gioithieu"></c:url>'><i></i>
								Giới thiệu</a></li>
						<li><i class="fa fa-angle-double-right"></i> <a href='<c:url value="/lienhe"></c:url>'><i></i>
								Liên hệ </a></li>
						
					</ul>
				</div>
				<div class="col-md-3" id="footer-support">
					<h3 class="tittle-footer">Liên hệ</h3>
					<ul>
						<li>

							<p>
								<i class="fa fa-home"
									style="font-size: 16px; padding-right: 5px;"></i> 67 QUAN HOA - CẦU GIẤY
							</p>
							<p>
								<i class="fa fa-home"
									style="font-size: 16px; padding-right: 5px;"></i> 324A LÝ THƯỜNG KIỆT
							</p>
							<p>
								<i class="sp-ic fa fa-mobile"
									style="font-size: 22px; padding-right: 5px;"></i> 0915550103
							</p>
							<p>
								<i class="sp-ic fa fa-envelope"
									style="font-size: 13px; padding-right: 5px;"></i>
								support@gmail.com
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section id="ft-bottom">
		<p class="text-center">Copyright © 2017 . Design by Tan Nhac Cu </p>
	</section>
</div>