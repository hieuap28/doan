<%@ page import="com.shopnhaccu.util.SecurityUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<div id="header">
	<div id="header-top">
		<div class="container">
			<div class="row clearfix">
				<div class="col-md-6" id="header-text">
				</div>
				<div class="col-md-6">
					<nav id="header-nav-top">
						<ul class="list-inline pull-right" id="headermenu">
							<security:authorize access="isAnonymous()">
								<li><a href='<c:url value="/dang-nhap"></c:url>'> Đăng
										nhập</a></li>
								<li><a href='<c:url value="/dang-ky"></c:url>'>Đăng ký</a></li>
							</security:authorize>
							<security:authorize access="isAuthenticated()">
								<li>Xin chào <%=SecurityUtils.getPrincipal().getFullname() %></li>
								<li><a href='<c:url value="/thoat"></c:url>'>Thoát</a></li>
							</security:authorize>


						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row" id="header-main">
			<div class="col-md-5">
				<form action='<c:url value="/search"></c:url>' class="form-inline" method="get">
					<div class="form-group">
						 <input type="text" name="keyword" id="keyword" placeholder=" Nhập sản phẩm cần tìm ..."
							class="form-control">
						<button type="submit" class="btn btn-default">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</form>
			</div>
			<div class="col-md-4">
				<a href=""> <img src='<c:url value="/template/image/logo.jpg"></c:url>'>
				</a>
			</div>
			<div class="col-md-3" id="header-right">
				<div class="pull-right">
					<div class="pull-left">
						<i class="glyphicon glyphicon-phone-alt"></i>
					</div>
					<div class="pull-right">
						<p id="hotline">HOTLINE</p>
						<p>0915.550.103</p>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>