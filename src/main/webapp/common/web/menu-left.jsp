<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<div class="col-md-3  fixside">
	<div class="box-left box-menu">
		<h3 class="box-title">
			<i class="fa fa-list"></i> Danh mục
		</h3>
		<ul>
			<c:forEach items="${listc}" var="c">
				<li><a
					href='<c:url value = "/category-detail?id=${c.id }"></c:url>'>${c.name }</a>
					<ul>
						<c:forEach items="${listb }" var="b">
							<li><a
								href='<c:url value="/brand-detail?id=${c.id }&idb=${b.id }"></c:url>'>${b.name }</a></li>
						</c:forEach>
					</ul></li>
			</c:forEach>
	</div>

	<div class="box-left box-menu">
		<h3 class="box-title">
			<i class="fa fa-list"></i> Sản phẩm bán chạy
		</h3>
		<!--  <marquee direction="down" onmouseover="this.stop()" onmouseout="this.start()"  > -->
		<ul>
			<c:forEach items="${list }" var="ph">
				<li class="clearfix"><a href='<c:url value="/chitietsanpham?id=${ph.id }"></c:url>'> <img
						src='<c:url value="/template/image/${ph.product_image}"></c:url>'
						class="img-responsive pull-left" width="80" height="80">
						<div class="info pull-right">
							<a href='<c:url value="/chitietsanpham?id=${ph.id }"></c:url>'><p class="name">${ph.product_name}</p></a>
							<b class="price"><fmt:formatNumber value="${ph.product_price}"
									type="number" />đ</b><br>
						</div>
				</a></li>
			</c:forEach>
		</ul>
		<!-- </marquee> -->
	</div>
	
	<div class="box-left box-menu">
		<h3 class="box-title">
			<i class="fa fa-list"></i> Sản phẩm mới
		</h3>
		<!--  <marquee direction="down" onmouseover="this.stop()" onmouseout="this.start()"  > -->
		<ul>
			<c:forEach items="${listh }" var="ph">
				<li class="clearfix"><a href='<c:url value="/chitietsanpham?id=${ph.id }"></c:url>'> <img
						src='<c:url value="/template/image/${ph.product_image}"></c:url>'
						class="img-responsive pull-left" width="80" height="80">
						<div class="info pull-right">
							<a href='<c:url value="/chitietsanpham?id=${ph.id }"></c:url>'><p class="name">${ph.product_name}</p></a>
							<b class="price"><fmt:formatNumber value="${ph.product_price}"
									type="number" />đ</b><br>
						</div>
				</a></li>
			</c:forEach>
		</ul>
		<!-- </marquee> -->
	</div>

	<div class="box-left box-menu">
		<h3 class="box-title">
			<i class="fa fa-list"></i> Liên hệ hỗ trợ
		</h3>
		<!--  <marquee direction="down" onmouseover="this.stop()" onmouseout="this.start()"  > -->
		<ul>

			<li class="clearfix"><a href=""> <img style="width: 40px;"
					src='<c:url value="/template/image/call1.png"></c:url>'
					class="img-responsive pull-left">
					<div class="info pull-right" style="margin: 0px -15px;">
						<p class="">Hà Nội: 67 Quan Hoa - Cầu Giấy</p>
						<b class="">0915.550.103</b>
					</div>
			</a></li>

			<li class="clearfix"><a href=""> <img style="width: 40px;"
					src='<c:url value="/template/image/call1.png"></c:url>'
					class="img-responsive pull-left">
					<div class="info pull-right" style="margin: 0px -15px;">
						<p class="">Tp.HCM: 324A Lý Thường Kiệt</p>
						<b class="">0915.550.103</b>
					</div>
			</a></li>

			<li class="clearfix"><a href=""> <img style="width: 40px;"
					src='<c:url value="/template/image/call1.png"></c:url>'
					class="img-responsive pull-left">
					<div class="info pull-right " style="margin-right: 85px;">
						<p>Bảo hành</p>
						<b>0915.550.103</b>

					</div>
			</a></li>

		</ul>
		<!-- </marquee> -->
	</div>
</div>