<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<div id="menunav">
	<div class="container">
		<nav>
			<div class="home pull-left">
				<a href='<c:url value="/home"></c:url>'>Trang chủ</a>
			</div>
			<!--menu main-->
			<ul id="menu-main">
				<li><a href='<c:url value="/gioithieu"></c:url>'>Giới thiệu</a></li>
				<li><a href='<c:url value="/lienhe"></c:url>'>Liên hệ </a></li>
				<li><a href='<c:url value="/baiviet"></c:url>'>Bài viết</a></li>
			</ul>
			<!-- end menu main-->

			<!--Shopping-->
			<ul class="pull-right" id="main-shopping">
				<li><a href='<c:url value="/view-cart"></c:url>'><i class="fa fa-shopping-basket"></i>Giỏ hàng </a></li>
			</ul>
			<!--end Shopping-->
		</nav>
	</div>
</div>