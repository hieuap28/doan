<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><dec:title default="Trang chủ" /></title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css"
	href='<c:url value="/template/web/css/font-awesome.min.css"></c:url>'>

<link rel="stylesheet" type="text/css"
	href='<c:url value="/template/web/css/bootstrap.min.css"></c:url>'>

<script
	src='<c:url value="/template/web/js/jquery-3.2.1.min.js"></c:url>'></script>

<script src='<c:url value="/template/web/js/bootstrap.min.js"></c:url>'></script>
<!---->
<link rel="stylesheet" type="text/css"
	href='<c:url value="/template/web/css/slick.css"></c:url>' />

<link rel="stylesheet" type="text/css"
	href='<c:url value="/template/web/css/slick-theme.css"></c:url>' />
<!--slide-->
<link rel="stylesheet" type="text/css"
	href='<c:url value="/template/web/css/style.css"></c:url>'>
<style type="text/css">
span {
	color: red;
	display: block;
}
</style>
</head>
<body>
	<div id="wrapper">
		<!-- header -->
		<%@ include file="/common/web/header.jsp"%>
		<!-- header -->
		<!-- menu -->
		<%@ include file="/common/web/menu.jsp"%>
		<!-- menu -->
		<div id="maincontent">
			<div class="container">
				<%@ include file="/common/web/menu-left.jsp"%>
				<dec:body />
			</div>
			<%@ include file="/common/web/footer.jsp"%>
		</div>
	</div>
	<script src='<c:url value="/template/web/js/slick.min.js"></c:url>'></script>
</body>

<!-- Messenger Plugin chat Code -->
<div id="fb-root"></div>
<script>
	window.fbAsyncInit = function() {
		FB.init({
			xfbml : true,
			version : 'v10.0'
		});
	};

	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id))
			return;
		js = d.createElement(s);
		js.id = id;
		js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>

<!-- Your Plugin chat code -->
<div class="fb-customerchat" attribution="page_inbox"
	page_id="106621294907900"></div>
</html>

<script type="text/javascript">
	$(function() {
		$hidenitem = $(".hidenitem");
		$itemproduct = $(".item-product");
		$itemproduct.hover(function() {
			$(this).children(".hidenitem").show(100);
		}, function() {
			$hidenitem.hide(500);
		})
	})
</script>

