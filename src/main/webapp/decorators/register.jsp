<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Đăng ký</title>

<!-- Custom fonts for this template-->
<link
	href='<c:url value = "/template/register/vendor/fontawesome-free/css/all.min.css"></c:url>'
	rel="stylesheet" type="text/css">
<!-- Custom styles for this template-->
<link href='<c:url value="/template/register/css/sb-admin.css"></c:url>'
	rel="stylesheet">
<style type="text/css">
span {
	color: red;
	display: block;
}
</style>
</head>

<body class="bg-dark">

	<dec:body />

	<!-- Bootstrap core JavaScript-->

	<script type="text/javascript"
		src='<c:url value="/template/register/js/all.js"></c:url>'>
		
	</script>
	<script
		src='<c:url value="/template/register/vendor/jquery/jquery.min.js"></c:url>'></script>
	<script
		src='<c:url value="/template/register/vendor/bootstrap/js/bootstrap.bundle.min.js"></c:url>'></script>
	<!-- Core plugin JavaScript-->
	<script
		src='<c:url value="/template/register/vendor/jquery-easing/jquery.easing.min.js"></c:url>'></script>

</body>

</html>
