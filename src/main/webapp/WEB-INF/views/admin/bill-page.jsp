<%@include file="/common/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Danh sách hóa đơn</title>
</head>

<body>
	<div class="main-content">
		<form action="<c:url value='/quan-tri/hoadon/danhsach'/>"
			id="formSubmit" method="get">

			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Trang
								chủ</a></li>
					</ul>
					<!-- /.breadcrumb -->
				</div>
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12">
							<c:if test="${not empty message}">
								<div class="alert alert-${alert}">${message}</div>
							</c:if>
							<div class="widget-box table-filter">
								<div class="table-btn-controls">
									<div class="pull-right tableTools-container">
										<div class="dt-buttons btn-overlap btn-group">
											<a flag="info"
												class="dt-button buttons-colvis btn btn-white btn-primary btn-bold"
												data-toggle="tooltip" title='Thêm bài viết'
												href='<c:url value='/quan-tri/init-brand-insert'></c:url>'>
												<span> <i class="fa fa-plus-circle bigger-110 purple"></i>
											</span>
											</a>

										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>Mã hóa đơn</th>
													<th>Người tạo</th>
													<th>Ngày tạo</th>
													<th>Tổng tiền</th>
													<th>Người nhận</th>
													<th>Địa chỉ nhận</th>
													<th>SDT</th>
													<th>Email</th>
													<th>Trạng thái</th>
													<th>Người sửa</th>
													<th>Ngày sửa</th>
													<th>Xem</th>
													<th>Cập nhật</th>
													<th>Xóa</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="item" items="${model.listResult}">
													<tr>
														<td>${item.id}</td>
														<td>${item.createdBy}</td>
														<td>${item.createdDate}</td>
														<td><fmt:formatNumber value="${item.totalmoney}" type="number"/></td>
														<td>${item.receiver}</td>
														<td>${item.address}</td>
														<td>${item.phone}</td>
														<td>${item.email}</td>
														<td>${item.status==0?"Chưa xác nhận":"Xác nhận"}</td>
														<td>${item.modifiedBy}</td>
														<td>${item.modifiedDate}</td>
														<td><a
															href="<c:url value="/quan-tri/bill-detail?id=${item.id}"></c:url>"><img
																src="<c:url value="/template/image/ic_detail.png"></c:url>" /></a>
														</td>
														<td><a
															href="<c:url value="/quan-tri/init-bill-update?id=${item.id}"></c:url>"><img
																src="<c:url value="/template/image/ic_update.png"></c:url>" /></a>
														</td>
														<td><a
															href="<c:url value="/quan-tri/bill-delete?id=${item.id}"></c:url>"><img
																src="<c:url value="/template/image/ic_delete.png"></c:url>" /></a>
														</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<!-- /.main-content -->
</body>
</html>