<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/common/taglib.jsp"%>
<html>
<head>
<title>Thêm bài viết</title>
</head>
<body>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs" id="breadcrumbs">
				<script type="text/javascript">
					try {
						ace.settings.check('breadcrumbs', 'fixed')
					} catch (e) {
					}
				</script>

				<ul class="breadcrumb">
					<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Home</a>
					</li>

					<li><a href="#">Forms</a></li>
					<li class="active">Form Elements</li>
				</ul>
				<!-- /.breadcrumb -->
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12">
						<c:if test="${not empty message}">
							<div class="alert alert-${alert}">${message}</div>
						</c:if>
						<h1>${msg}</h1>
						<form action='<c:url value='/quan-tri/new-insert'></c:url>' method="POST"
							modelAttribute="n" enctype="multipart/form-data">
							<table class="table table-bordered" id="dataTable" width="100%"
								cellspacing="0">
								<tr>
									<th>Tiêu đề</th>
									<td><input name="title" id="title" /></td>
								</tr>
								<tr>
									<th>Hình đại diện</th>
									<td><input type="file" name="image" id="image" /></td>
								</tr>
								
								<tr>
									<th>Mô tả ngắn</th>
									<td><input name="shortdescription" id="shortdescription" /></td>
								</tr>
								<tr>
									<th>Nội dung</th>
									<td><textarea rows="" cols="" id="content" name="content" style="width: 820px; height: 175px;"></textarea></td>
								</tr>
								
								<tr>
									<td></td>
									<td><input type="submit" value="Thêm mới" /> <input
										type="reset" value="Làm lại" /></td>
								</tr>
								
								<tr>
									<td><a href="">Go back</a></td>
									<td></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<script>
		var editor = '';
		$(document).ready(function(){
			editor = CKEDITOR.replace("content");
		});
	</script>
</body>
</html>