<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/common/taglib.jsp"%>
<html>
<head>
<title>Thêm bài viết</title>
</head>
<body>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs" id="breadcrumbs">
				<script type="text/javascript">
					try {
						ace.settings.check('breadcrumbs', 'fixed')
					} catch (e) {
					}
				</script>

				<ul class="breadcrumb">
					<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Home</a>
					</li>

					<li><a href="#">Forms</a></li>
					<li class="active">Form Elements</li>
				</ul>
				<!-- /.breadcrumb -->
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12">
						<c:if test="${not empty msg}">
							<div class="alert alert-${alert}">${msg}</div>
						</c:if>
						${msg}
						<form:form action="product-insert" method="POST"
							modelAttribute="p" enctype="multipart/form-data">
							<table class="table table-bordered" id="dataTable" width="100%"
								cellspacing="0">
								<tr>
									<th>Tên</th>
									<td><form:input path="product_name" /></td>
								</tr>
								<tr>
									<th>Mô tả</th>
									<td><form:input path="product_description" /></td>
								</tr>
								<tr>
									<th>Loại</th>
									<td><form:select path="category_id">
											<c:forEach items="${listc}" var="c">
												<form:option value="${c.id}">${c.name}</form:option>
											</c:forEach>
										</form:select></td>
								</tr>
								<tr>
									<th>Hãng</th>
									<td><form:select path="brand_id">
											<c:forEach items="${listb}" var="b">
												<form:option value="${b.id}">${b.name}</form:option>
											</c:forEach>
										</form:select></td>
								</tr>
								<tr>
									<th>Số lượng</th>
									<td><form:input step="1" value="1" type="number"
											path="product_amount" />
									</td>
								</tr>
								<tr>
									<th>Giá tiền</th>
									<td><form:input path="product_price" step="1000" value="1000"
											type="number" />
									</td>
								</tr>

								<tr>
									<th>Ảnh đại diện</th>
									<td><form:input type="file" path="product_image" /></td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" value="Thêm mới" /> <input
										type="reset" value="Làm lại" /></td>
								</tr>
								<tr>
									<td><a href='<c:url value='/quan-tri/sanpham/danhsach?page=1&limit=4'></c:url>'>Go back</a></td>
									<td></td>
								</tr>
							</table>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>