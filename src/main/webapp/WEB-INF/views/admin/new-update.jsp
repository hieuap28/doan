<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/common/taglib.jsp"%>
<html>
<head>
<title>Thêm bài viết</title>
</head>
<body>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs" id="breadcrumbs">
				<script type="text/javascript">
					try {
						ace.settings.check('breadcrumbs', 'fixed')
					} catch (e) {
					}
				</script>

				<ul class="breadcrumb">
					<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Home</a>
					</li>

					<li><a href="#">Forms</a></li>
					<li class="active">Form Elements</li>
				</ul>
				<!-- /.breadcrumb -->
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12">
						<c:if test="${not empty message}">
							<div class="alert alert-${alert}">${message}</div>
						</c:if>
						<h1>${msg}</h1>
						<form:form action="new-update" method="POST"
							modelAttribute="n" enctype="multipart/form-data">
							<table class="table table-bordered" id="dataTable" width="100%"
								cellspacing="0">
								<form:input path="id" hidden="hidden" />
								<tr>
									<th>Tiêu đề</th>
									<td><form:input path="title" /></td>
								</tr>
								<tr>
									<th>Ảnh đại diện</th>
									<td><img width="100px"
										src='<c:url value="/template/image/${n.image }"></c:url>' />
										<input name="imagelinkold" value="${n.image}"
										hidden="hidden" /> <form:input type="file"
											path="image" /></td>
								</tr>
								<tr>
									<th>Mô tả ngắn</th>
									<td>
									<form:input path="shortdescription" /></td>
								</tr>
								<tr>
									<th>Nội dung</th>
									<td>
										<form:textarea path="content"/>
									</td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" value="Cập nhật" /> <input
										type="reset" value="Làm lại" /></td>
								</tr>
								<tr>
									<td><a
										href='<c:url value='/quan-tri/baiviet/danhsach?page=1&limit=2'></c:url>'>Go
											back</a></td>
									<td></td>
								</tr>
							</table>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<script>
		var editor = '';
		$(document).ready(function(){
			editor = CKEDITOR.replace("content");
		});
	</script>
</body>
</html>