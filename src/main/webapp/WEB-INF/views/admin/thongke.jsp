<%@include file="/common/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Danh sách hóa đơn</title>
</head>

<body>
	<div class="main-content">


		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Trang
							chủ</a></li>
				</ul>
				<!-- /.breadcrumb -->
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12">
						<c:if test="${not empty message}">
							<div class="alert alert-${alert}">${message}</div>
						</c:if>
						<div class="row">
							<div class="col-xs-6">
								<form action='<c:url value="/quan-tri/thongke/sanpham"></c:url>'>
									<label>Chọn tháng</label><select name="thang">
										<option>--Chọn tháng--</option>
										<option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
										<option>6</option>
										<option>7</option>
										<option>8</option>
										<option>9</option>
										<option>10</option>
										<option>11</option>
										<option>12</option>
									</select>
							</div>
							<div class="col-xs-6">
								<label>Nhập năm</label> <input name="nam" type="text" />
								<button>Tìm kiếm</button>
							</div>
							</form>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="table-responsive">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>Mã sản phẩm</th>
												<th>Tên sản phẩm</th>
												<th>Số lượng bán ra</th>
												<th>Giá tiền mỗi cái</th>
												<th>Tổng tiền bán ra của mỗi sản phẩm</th>
											</tr>
										</thead>
										<tbody>
											<c:set var="sum" value="0"></c:set>
											<c:forEach var="t" items="${list }">
												<c:set var="sum" value="${ sum+t.tongtien}"></c:set>
												<tr>
													<td>${t.idproduct}</td>
													<td>${t.product_name}</td>
													<td>${t.product_amount}</td>
													<td><fmt:formatNumber value="${t.product_price}"
															type="number" /></td>
													<td><fmt:formatNumber value="${t.tongtien}"
															type="number" /></td>
												</tr>
											</c:forEach>
										</tbody>
										<tfoot>
											<tr>
												<th style="text-align: end;" colspan="4">Tổng tiền bán
													ra trong tháng :</th>
												<th><fmt:formatNumber value="${sum }" type="number" /></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.main-content -->
</body>
</html>