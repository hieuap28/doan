<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/common/taglib.jsp"%>
<html>
<head>
<title>Thêm bài viết</title>
</head>
<body>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs" id="breadcrumbs">
				<script type="text/javascript">
					try {
						ace.settings.check('breadcrumbs', 'fixed')
					} catch (e) {
					}
				</script>

				<ul class="breadcrumb">
					<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Home</a>
					</li>

					<li><a href="#">Forms</a></li>
					<li class="active">Form Elements</li>
				</ul>
				<!-- /.breadcrumb -->
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12">
						<c:if test="${not empty msg}">
							<div class="alert alert-${alert}">${msg}</div>
						</c:if>
						${msg}
						<form:form action="user-insert" method="POST"
							modelAttribute="u">
							<table class="table table-bordered" id="dataTable" width="100%"
								cellspacing="0">
								<input type="text" id="status" name="status" hidden="hidden"
									value="1" />
								<tr>
									<th>Tên người dùng</th>
									<td><form:input path="fullname" /></td>
								</tr>
								<tr>
									<th>Tài khoản</th>
									<td><form:input path="username" /></td>
								</tr>
								<tr>
									<th>Mật khẩu</th>
									<td><form:input path="password" /></td>
								</tr>
								<tr>
									<th>Chức vụ</th>
									<td>
									</td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" value="Thêm mới" /> <input
										type="reset" value="Làm lại" /></td>
								</tr>
								<tr>
									<td><a
										href='<c:url value='/quan-tri/taikhoan/danhsach?page=1&limit=4'></c:url>'>Go
											back</a></td>
									<td></td>
								</tr>
							</table>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>