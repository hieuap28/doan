<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/common/taglib.jsp"%>
<html>
<head>
<title>Thêm bài viết</title>
</head>
<body>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs" id="breadcrumbs">
				<script type="text/javascript">
					try {
						ace.settings.check('breadcrumbs', 'fixed')
					} catch (e) {
					}
				</script>

				<ul class="breadcrumb">
					<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Home</a>
					</li>

					<li><a href="#">Forms</a></li>
					<li class="active">Form Elements</li>
				</ul>
				<!-- /.breadcrumb -->
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12">
						<c:if test="${not empty message}">
							<div class="alert alert-${alert}">${message}</div>
						</c:if>
						<h1>${msg}</h1>
						<form:form action="category-update" method="POST" modelAttribute="c"
							>
							<table class="table table-bordered" id="dataTable" width="100%"
								cellspacing="0">
								<form:input path="id" hidden="hidden" />
								<tr>
									<th>Tên</th>
									<td><form:input path="name" /></td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" value="Cập nhật" /> <input
										type="reset" value="Làm lại" /></td>
								</tr>
								<tr>
									<td><a
										href='<c:url value='/quan-tri/loaisanpham/danhsach'></c:url>'>Go
											back</a></td>
									<td></td>
								</tr>
							</table>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>