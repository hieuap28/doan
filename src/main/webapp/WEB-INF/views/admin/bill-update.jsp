<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/common/taglib.jsp"%>
<html>
<head>
<title>Thêm bài viết</title>
</head>
<body>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs" id="breadcrumbs">
				<script type="text/javascript">
					try {
						ace.settings.check('breadcrumbs', 'fixed')
					} catch (e) {
					}
				</script>

				<ul class="breadcrumb">
					<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Home</a>
					</li>

					<li><a href="#">Forms</a></li>
					<li class="active">Form Elements</li>
				</ul>
				<!-- /.breadcrumb -->
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12">
						<c:if test="${not empty message}">
							<div class="alert alert-${alert}">${message}</div>
						</c:if>
						<h1>${msg}</h1>
						<form:form action="bill-update" method="POST" modelAttribute="b">
							<table class="table table-bordered" id="dataTable" width="100%"
								cellspacing="0">
								<form:input path="id" hidden="hidden" />
								<form:input path="totalmoney" hidden="hidden" /> 
								<tr>
									<th>Người nhận</th>
									<td><form:input path="receiver" /></td>
								</tr>
								<tr>
									<th>Địa chỉ nhận</th>
									<td><form:input path="address" /></td>
								</tr>
								<tr>
									<th>Số điện thoại</th>
									<td><form:input path="phone" /></td>
								</tr>
								<tr>
									<th>Email</th>
									<td><form:input path="email" /></td>
								</tr>
								<tr>
									<th>Trạng thái</th>
									<td><form:radiobutton path="status" value="0" /> <form:label
											path="status">Chưa xác nhận</form:label> <form:radiobutton
											path="status" value="1" /> <form:label path="status">Xác nhận</form:label></td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" value="Cập nhật" /> <input
										type="reset" value="Làm lại" /></td>
								</tr>
								<tr>
									<td><a
										href='<c:url value='/quan-tri/hoadon/danhsach'></c:url>'>Go
											back</a></td>
									<td></td>
								</tr>
							</table>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>