<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/common/taglib.jsp"%>
<html>
<head>
<title>Thêm bài viết</title>
</head>
<body>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs" id="breadcrumbs">
				<script type="text/javascript">
					try {
						ace.settings.check('breadcrumbs', 'fixed')
					} catch (e) {
					}
				</script>

				<ul class="breadcrumb">
					<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Home</a>
					</li>

					<li><a href="#">Forms</a></li>
					<li class="active">Form Elements</li>
				</ul>
				<!-- /.breadcrumb -->
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12">
						<c:if test="${success==true }">
							<h1 style="color: green;">${msg}</h1>
						</c:if>
						<c:if test="${success==false }">
							<h1 style="color: red">${msg}</h1>
						</c:if>
						<form action='<c:url value='/quan-tri/category-insert'></c:url>'
							method="POST" modelAttribute="c">
							<table class="table table-bordered" id="dataTable" width="100%"
								cellspacing="0">

								<tr>
									<th>Tên loại sản phẩm</th>
									<td><input name="name" id="name" /></td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" value="Thêm mới" /> <input
										type="reset" value="Làm lại" /></td>
								</tr>

								<tr>
									<td><a
										href='<c:url value="/quan-tri/loaisanpham/danhsach"></c:url>'>Go
											back</a></td>
									<td></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>