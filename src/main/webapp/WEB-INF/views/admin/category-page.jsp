<%@include file="/common/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Danh sách loại sản phẩm</title>
</head>

<body>
	<div class="main-content">
		<form action="<c:url value='/quan-tri/loaisanpham/danhsach'/>"
			id="formSubmit" method="get">

			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Trang
								chủ</a></li>
					</ul>
					<!-- /.breadcrumb -->
				</div>
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12">
							<c:if test="${not empty message}">
								<div class="alert alert-${alert}">${message}</div>
							</c:if>
							<div class="widget-box table-filter">
								<div class="table-btn-controls">
									<div class="pull-right tableTools-container">
										<div class="dt-buttons btn-overlap btn-group">
											<a flag="info"
												class="dt-button buttons-colvis btn btn-white btn-primary btn-bold"
												data-toggle="tooltip" title='Thêm bài viết'
												href='<c:url value='/quan-tri/init-category-insert'></c:url>'>
												<span> <i class="fa fa-plus-circle bigger-110 purple"></i>
											</span>
											</a>
											
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="table-responsive">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>Tên loại sản phẩm</th>
													<th>Người tạo</th>
													<th>Ngày tạo</th>
													<th>Người cập nhật</th>
													<th>Ngày cập nhật</th>
													<th>Sửa</th>
													<th>Xóa</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="item" items="${model.listResult}">
													<tr>
														<td>${item.name}</td>
														<td>${item.createdBy}</td>
														<td>${item.createdDate}</td>
														<td>${item.modifiedBy}</td>
														<td>${item.modifiedDate}</td>
														<td>
																<a href='<c:url value="/quan-tri/init-category-update?id=${item.id}"></c:url>'><img src="<c:url value="/template/image/ic_update.png"></c:url>"/></a>
															</td>
															<td>
																<a href="<c:url value="/quan-tri/category-delete?id=${item.id}"></c:url>"><img src="<c:url value="/template/image/ic_delete.png"></c:url>"/></a>
															</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<!-- /.main-content -->
</body>
</html>