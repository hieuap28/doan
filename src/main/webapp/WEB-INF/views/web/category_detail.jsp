<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<%@ page import="com.shopnhaccu.util.SecurityUtils"%>
<div class="col-md-9 bor">
	<section id="slide" class="text-center">
		<img width="100%"
			src="<c:url value="/template/image/banner.jpg"></c:url>"
			class="img-thumbnail">
	</section>
	<section class="box-main1">
		<h3 class="title-main" style="text-align: center;">
			<a href="javascript:void(0)">${c.name}</a>
		</h3>
		<c:forEach items="${listp}" var="p">
			<div class="showitem" style="margin-top: 10px; margin-bottom: 10px">
				<div class="col-md-3 item-product bor">
					<a href='<c:url value="/chitietsanpham?id=${p.id }"></c:url>'> <img
						src='<c:url value="/template/image/${p.product_image }"></c:url>'
						class="" width="100%" height="180">
					</a>
					<div class="info-item">
						<a href='<c:url value="/chitietsanpham?id=${p.id }"></c:url>'>${p.product_name}</a>
						<p>
							<b class="price"><fmt:formatNumber value="${p.product_price}"
									type="number" /> đ</b>
						</p>
					</div>
					<div class="hidenitem">
						<p>
							<a href'<c:url value="/chitietsanpham?id=${p.id }"></c:url>'><i class="fa fa-search"></i></a>
						</p>
						<p>
								<a onclick="return confirm('Bạn muốn thêm ${p.product_name} ')"
									href='<c:url value="/add-cart?idp=${p.id }"></c:url>'><i
									class="fa fa-shopping-basket"></i></a>
						</p>
					</div>
				</div>
			</div>
		</c:forEach>
	</section>
</div>
</div>
