<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<div class="col-md-9 bor">

	<section class="box-main1">
		<h3 class="title-main" style="text-align: center;">
			<a href="javascript:void(0)">Giới thiệu </a>
		</h3>
		<b style="font-size: 20px; color:">Giới thiệu về cửa hàng</b>
		<p style="padding: 13px 0px; font-size: 15px">Với kinh nghiệm trên
			20 năm phục vụ từ Nam ra Bắc, chúng tôi tự hào mang lại những giá trị
			lớn lao cho cộng đồng. Mỗi một sản phẩm hoặc dịch vụ đưa ra cộng
			đồng, chúng tôi đặt uy tín và trách nhiệm trên hết. Một sản phẩm xấu
			hoăc dịch vụ tồi sẽ giết chết thương hiệu chúng tôi dày công vun đắp.</p>
		<img src="<c:url value="/template/image/gioithieu.jpg"></c:url>" /> <br>
		<b style="margin: 13px 0px; font-size: 15px; color: green">Shop
			chúng tôi mang tới cho bạn nhiều sự lựa chọn:</b>
		<p style="padding: 13px 0px; font-size: 15px">Khi bạn muốn mua Đàn
			Piano, Đàn Guitar, Saxophone, Organ, Âm thanh... Tân Nhạc Cụ mang tới
			nhiều sự lựa chọn với rất nhiều các thương hiệu quốc tế như Essex
			Piano, Kawai, Yamaha, Martin, Epiphone, Ibanez, Cort, Codoba, Rode,
			Audix, Cordial...</p>
		<img style="padding-left: 100px"
			src="<c:url value="/template/image/gioithieu2.jpg"></c:url>" /><br>
		<p style="padding: 13px 0px; font-size: 15px">Bạn có thể lấy đàn
			mới 100%, mang tới nhà mới đập hộp sau khi trải nghiệm trên những cây
			đàn trưng bày. Gọi ngay hotline: 03.794.88.177 để được tư vấn chi
			tiết nhất.</p>
	</section>

</div>