<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<%@ page import="com.shopnhaccu.util.SecurityUtils"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Trang chủ</title>
</head>
<body>
	<div class="col-md-9 bor">
		<section id="slide" class="text-center">
			<img src='<c:url value="/template/image/banner.jpg"></c:url>'
				class="img-thumbnail">
		</section>
			<section class="box-main1">
				<h3 class="title-main" style="text-align: center;">
					<a href="">GUITAR ACOUSTIC </a>
				</h3>

				<div class="showitem">

					<c:forEach items="${listgt }" var="gta">	
							<div class="col-md-3 item-product bor">
								<a href='<c:url value="/chitietsanpham?id=${gta.id }"></c:url>'>
									<img
									src='<c:url value="/template/image/${gta.product_image }"></c:url>'
									class="" width="100%" height="180">
								</a>
								<div class="info-item">
									<a href='<c:url value="/chitietsanpham?id=${gta.id }"></c:url>'>${gta.product_name}</a>
									<p>
										<b class="price"><fmt:formatNumber
												value="${gta.product_price}" type="number" />đ</b>
									</p>
								</div>
								<div class="hidenitem">
									<p>
										<a href='<c:url value="/chitietsanpham?id=${gta.id }"></c:url>'><i class="fa fa-search"></i></a>
									</p>
									<p>
										<a
											onclick="return confirm('Bạn muốn thêm ${gta.product_name} ')"
											href='<c:url value="/add-cart?idp=${gta.id }"></c:url>'><i
											class="fa fa-shopping-basket"></i></a>
									</p>
								</div>
							</div>
					</c:forEach>

				</div>
			</section>
			
			<section class="box-main1">
				<h3 class="title-main" style="text-align: center;">
					<a href="">ĐÀN PIANO </a>
				</h3>

				<div class="showitem">

					<c:forEach items="${listpiano }" var="pn">
						
							<div class="col-md-3 item-product bor">
								<a href='<c:url value="/chitietsanpham?id=${pn.id }"></c:url>'>
									<img
									src='<c:url value="/template/image/${pn.product_image }"></c:url>'
									class="" width="100%" height="180">
								</a>
								<div class="info-item">
									<a href='<c:url value="/chitietsanpham?id=${pn.id }"></c:url>'>${pn.product_name}</a>
									<p>
										<b class="price"><fmt:formatNumber
												value="${pn.product_price}" type="number" />đ</b>
									</p>
								</div>
								<div class="hidenitem">
									<p>
										<a href='<c:url value="/chitietsanpham?id=${gta.id }"></c:url>'><i class="fa fa-search"></i></a>
									</p>
									<p>
										<a
											onclick="return confirm('Bạn muốn thêm ${pn.product_name} ')"
											href='<c:url value="/add-cart?idp=${pn.id }"></c:url>'><i
											class="fa fa-shopping-basket"></i></a>
									</p>
								</div>
							</div>
					</c:forEach>

				</div>
			</section>
			
			<section class="box-main1">
				<h3 class="title-main" style="text-align: center;">
					<a href="">ĐÀN ORGAN </a>
				</h3>

				<div class="showitem">

					<c:forEach items="${listog }" var="og">
						
							<div class="col-md-3 item-product bor">
								<a href='<c:url value="/chitietsanpham?id=${og.id }"></c:url>'>
									<img
									src='<c:url value="/template/image/${og.product_image }"></c:url>'
									class="" width="100%" height="180">
								</a>
								<div class="info-item">
									<a href='<c:url value="/chitietsanpham?id=${og.id }"></c:url>'>${og.product_name}</a>
									<p>
										<b class="price"><fmt:formatNumber
												value="${og.product_price}" type="number" />đ</b>
									</p>
								</div>
								<div class="hidenitem">
									<p>
										<a href='<c:url value="/chitietsanpham?id=${gta.id }"></c:url>'><i class="fa fa-search"></i></a>
									</p>
									<p>
										<a
											onclick="return confirm('Bạn muốn thêm ${og.product_name} ')"
											href='<c:url value="/add-cart?idp=${og.id }"></c:url>'><i
											class="fa fa-shopping-basket"></i></a>
									</p>
								</div>
							</div>
					</c:forEach>

				</div>
			</section>
			
			<section class="box-main1">
				<h3 class="title-main" style="text-align: center;">
					<a href="">TRỐNG </a>
				</h3>

				<div class="showitem">

					<c:forEach items="${listt }" var="t">
						
							<div class="col-md-3 item-product bor">
								<a href='<c:url value="/chitietsanpham?id=${t.id }"></c:url>'>
									<img
									src='<c:url value="/template/image/${t.product_image }"></c:url>'
									class="" width="100%" height="180">
								</a>
								<div class="info-item">
									<a href='<c:url value="/chitietsanpham?id=${t.id }"></c:url>'>${t.product_name}</a>
									<p>
										<b class="price"><fmt:formatNumber
												value="${t.product_price}" type="number" />đ</b>
									</p>
								</div>
								<div class="hidenitem">
									<p>
										<a href='<c:url value="/chitietsanpham?id=${gta.id }"></c:url>'><i class="fa fa-search"></i></a>
									</p>
									<p>
										<a
											onclick="return confirm('Bạn muốn thêm ${t.product_name} ')"
											href='<c:url value="/add-cart?idp=${t.id }"></c:url>'><i
											class="fa fa-shopping-basket"></i></a>
									</p>
								</div>
							</div>
					</c:forEach>

				</div>
			</section>
			
			<section class="box-main1">
				<h3 class="title-main" style="text-align: center;">
					<a href="">KÈN-SÁO </a>
				</h3>

				<div class="showitem">

					<c:forEach items="${listk }" var="k">
						
							<div class="col-md-3 item-product bor">
								<a href='<c:url value="/chitietsanpham?id=${k.id }"></c:url>'>
									<img
									src='<c:url value="/template/image/${k.product_image }"></c:url>'
									class="" width="100%" height="180">
								</a>
								<div class="info-item">
									<a href='<c:url value="/chitietsanpham?id=${k.id }"></c:url>'>${k.product_name}</a>
									<p>
										<b class="price"><fmt:formatNumber
												value="${k.product_price}" type="number" />đ</b>
									</p>
								</div>
								<div class="hidenitem">
									<p>
										<a href='<c:url value="/chitietsanpham?id=${gta.id }"></c:url>'><i class="fa fa-search"></i></a>
									</p>
									<p>
										<a
											onclick="return confirm('Bạn muốn thêm ${pn.product_name} ')"
											href='<c:url value="/add-cart?idp=${pn.id }"></c:url>'><i
											class="fa fa-shopping-basket"></i></a>
									</p>
								</div>
							</div>
					</c:forEach>

				</div>
			</section>

	</div>
</body>
</html>