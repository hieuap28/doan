<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<div class="col-md-9 bor">
	<section class="box-main1">
		<h3 class="title-main">
			<a href=""> Thanh toán đơn hàng </a>
		</h3>
		<form:form modelAttribute="bill" action="thanhtoan" method="POST"
			cssClass="form-horizontal formcustome" role="form"
			style="margin-top: 20px;">
			<input type="text" id="status" name="status" hidden="hidden" value="0"/>
			<div class="form-group">
				<form:label path="receiver" cssClass="col-md-2 col-md-offset-1">Tên người nhận</form:label>

				<div class="col-md-8">
					<input name="receiver" id="receiver" class="form-control" /><span id=receiver_error></span>
				</div>
			</div>
			<div class="form-group">
				<form:label path="email" cssClass="col-md-2 col-md-offset-1">Email</form:label>

				<div class="col-md-8">
					<form:input path="email" cssClass="form-control"></form:input><span id="email_error"></span>
				</div>
			</div>
			<div class="form-group">
				<form:label path="phone" cssClass="col-md-2 col-md-offset-1">Số điện thoại</form:label>

				<div class="col-md-8">
					<form:input path="phone" cssClass="form-control"></form:input><span id="phone_error"></span>
				</div>
			</div>
			<div class="form-group">
				<form:label path="totalmoney" cssClass="col-md-2 col-md-offset-1">Tổng tiền</form:label>
				<div class="col-md-8">
					<input readonly="" value="${tongtien}" name="totalmoney"
						id="totalmoney" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<form:label path="address" cssClass="col-md-2 col-md-offset-1">Địa chỉ</form:label>
				<div class="col-md-8">
					<form:input path="address" cssClass="form-control"></form:input>
				</div>
			</div>
			<form:button type="submit" style="margin-bottom: 20px;"
				class="btn btn-success col-md-2 col-md-offset-5"
				id="btn btn-success col-md-2 col-md-offset-5" onclick="return validate();">Xác nhận</form:button>
		</form:form>

	</section>
</div>
</div>

<script type="text/javascript"
		src='<c:url value="/template/web/js/validate.js"></c:url>'>