<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<div class="col-md-9 bor">
	<section class="box-main1">
		<h3 class="title-main">
			<a href=""> Thông báo thanh toán </a>
		</h3>

		<div class="alert alert-success">Lưu thông tin đơn hành thành
			công ! Chúng tôi sẽ liên hệ với bạn sớm nhất !</div>

		<a href='<c:url value="/home"></c:url>' class="btn btn-success">Trở về trang chủ</a>
	</section>
</div>