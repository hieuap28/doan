<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@include file="/common/taglib.jsp"%>
<div class="col-md-9 bor">
	<section class="box-main1">
		<h3 class="title-main">
			<a href=""> Giỏ hàng của bạn </a>

		</h3>
		<form action='<c:url value="/update-cart"></c:url>' method="post">
			<table class="table table-hover">
				<thead>
					<tr>
						<th></th>
						<th>Tên sản phẩm</th>
						<th>Số lượng <input type="submit" value="Cập nhật giỏ hàng"></th>
						<th>Giá</th>
						<th>Tổng tiền</th>
						<th>Thao tác</th>
					</tr>
				</thead>
				<tbody>

					<c:forEach items="${listp}" var="p">

						<tr>
							<td><img width="80px"
								src='<c:url value="/template/image/${p.product_image }"></c:url>'
								alt=""></td>
							<td>${p.product_name}</td>
							<td><input style="width: 20%" name="product_amount" id="product_amount"
								step="1" value="${p.product_amount }" type="number" /></td>
							<td><b class="price"><fmt:formatNumber
										value="${p.product_price}" type="number" /> đ</b></td>
							<td><b class="price"><fmt:formatNumber
										value="${p.product_price*p.product_amount}" type="number" />
									đ</b></td>
							<td><a
								href='<c:url value="/card-delete?idp=${p.id }"></c:url>'
								class="btn btn-xs btn-danger"><i class="fa fa-remove"></i>
									Xóa</a></td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</form>

		<div class="col-md-5 pull-right">
			<ul class="list-group">
				<li class="list-group-item">
					<h3>Thông tin đơn hàng</h3>
				</li>

				<li class="list-group-item"><span class="badge"></span> Số tiền
					: <b class="price"><fmt:formatNumber value="${tongtien}"
							type="number" /> đ</b></li>



				<li class="list-group-item"><span class="badge"></span> Tổng số
					tiền thanh toán: <b class="price"><fmt:formatNumber
							value="${tongtien}" type="number" /> đ</b></li>

				<li class="list-group-item"><a
					href='<c:url value="/home"></c:url>' class="btn btn-success">Tiếp
						tục mua hàng</a> <a href='<c:url value="/init-thanhtoan"></c:url>'
					class="btn btn-success">Thanh toán</a></li>

			</ul>
		</div>
	</section>
</div>
</div>
</div>
