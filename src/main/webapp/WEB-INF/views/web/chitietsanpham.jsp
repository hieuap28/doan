<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Chi tiết sản phẩm</title>
</head>
<body>
	<div class="col-md-9 bor">
		<section class="box-main1">
			<div class="col-md-6 text-center">

				<img src="<c:url value="/template/image/${p.product_image}"></c:url>"
					class="img-responsive bor" id="imgmain" width="100%" height="370"
					data-zoom-image="images/16-270x270.png">

			</div>
			<div class="col-md-6 bor" style="margin-top: 20px; padding: 30px;">
				<ul id="right">
					<li><h3>${p.product_name}</h3></li>

					<li><p>
							<b class="price"><fmt:formatNumber value="${p.product_price}"
									type="number" /> đ</b></li>
					<li><a onclick="return confirm('Bạn muốn thêm ${p.product_name} ')"
						href="add-cart.htm?idp=${p.id}" class="btn btn-default"> <i
							class="fa fa-shopping-basket"></i>Add TO Cart
					</a></li>
				</ul>
			</div>

		</section>
		<div class="col-md-12" id="tabdetail">
			<div class="row">

				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#home">Mô tả
							sản phẩm </a></li>
					
				</ul>
				<div class="tab-content">
					<div id="home" class="tab-pane fade in active">
						<h3>Nội dung</h3>
						<p>${p.product_description}</p>
					</div>
				</div>
			</div>
		</div>
		
	</div>



</body>
</html>