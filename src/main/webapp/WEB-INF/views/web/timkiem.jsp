<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<%@ page import="com.shopnhaccu.util.SecurityUtils"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>San pham</title>
</head>
<body>
	<div class="col-md-9 bor">
		<section id="slide" class="text-center">
			<img src='<c:url value="/template/image/banner.jpg"></c:url>'
				class="img-thumbnail">
		</section>
			<section class="box-main1">

				<div class="showitem">

					<c:forEach items="${result }" var="rs">
						
							<div class="col-md-3 item-product bor">
								<a href='<c:url value="/chitietsanpham?id=${rs.id }"></c:url>'>
									<img
									src='<c:url value="/template/image/${rs.product_image }"></c:url>'
									class="" width="100%" height="180">
								</a>
								<div class="info-item">
									<a href='<c:url value="/chitietsanpham?id=${rs.id }"></c:url>'>${rs.product_name}</a>
									<p>
										<b class="price"><fmt:formatNumber
												value="${rs.product_price}" type="number" />đ</b>
									</p>
								</div>
								<div class="hidenitem">
									<p>
										<a href=""><i class="fa fa-search"></i></a>
									</p>
									<p>
										<a href=""><i class="fa fa-heart"></i></a>
									</p>
									<p>
										<a
											onclick="return confirm('Bạn muốn thêm ${rs.product_name} ')"
											href='<c:url value="/add-cart?idp=${rs.id }"></c:url>'><i
											class="fa fa-shopping-basket"></i></a>
									</p>
								</div>
							</div>
					</c:forEach>

				</div>
			</section>

	</div>



</body>
</html>