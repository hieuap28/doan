<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Bài viết</title>
</head>
<body>
	<div class="col-md-9 bor">
	
		<section class="box-main1">
			<h3 class="title-main" style="text-align: center;">
				<a href="javascript:void(0)">Tin tức </a>
			</h3>
			<div style="margin-top: 50px">
			<ul>
				<c:forEach items="${listn}" var="n">
					<li style="list-style: none;clear:left;border-bottom: 2px solid #ccc;
					float: left;margin-bottom: 20px;"><img style="width: 34%; height: 190px;float: left;margin-bottom: 20px;margin-right: 10px;"
						src="<c:url value="/template/image/${n.image}"></c:url>" alt="" />
						<b><a style="font-size: 20px;" href='<c:url value="/new-detail?id=${n.id }"></c:url>'>${n.title }</a></b><br> <i
						style="padding: 12px 0px; font-size: 15px" class="fa fa-clock-o"
						aria-hidden="true">${n.createdDate}</i><br>
						<p style="font-size: 16px">${n.shortdescription}</p></li>
				</c:forEach>
			</ul>
			</div>
		</section>

	</div>
</body>
</html>