<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<div class="container">
	<div class="card card-register mx-auto mt-5">
		<div class="card-header">Đăng ký tài khoản</div>
		<div class="card-body">
			<form action='<c:url value="/dang-ky/user-insert"></c:url>'
				method="POST" modelAttribute="u">
				<h2 style="color: green;">${msg }</h2>
				<input type="text" id="status" name="status" hidden="hidden"
					value="1">
				<div class="form-group">
					<div class="form-group">
						<div class="form-label-group">
							<input type="text" id="fullname" name="fullName" class="form-control"
								placeholder="Họ và tên " required="required"> <label
								for="fullname">Họ và tên</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="form-label-group">
						<input type="text" id="username" name="userName" class="form-control"
							placeholder="Tài khoản " required="required"> <label
							for="username">Tài khoản</label> <span id="username_error"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="form-row">
						<div class="col-md-6">
							<div class="form-label-group">
								<input type="password" id="password" name="password" class="form-control"
									placeholder="Mật khẩu" required="required"> <label
									for="password">Mật khẩu</label> <span id="password_error"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-label-group">
								<input type="password" id="repassword" class="form-control"
									placeholder="Nhập lại mật khẩu" required="required"> <label
									for="repassword">Nhập lại mật khẩu</label> <span
									id="repassword_error"></span>
							</div>
						</div>
					</div>
				</div>
				<input type="submit" class="btn btn-primary btn-block"
					onclick="return validate();" name="" value="Đăng ký">
			</form>
			<div class="text-center">
				<a class="d-block small mt-3"
					href='<c:url value="/dang-nhap"></c:url>'>Đăng nhập</a>
			</div>
		</div>
	</div>
</div>