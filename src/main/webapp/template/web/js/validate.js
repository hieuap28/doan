function getValue(id) {
	return document.getElementById(id).value.trim();
}

function showError(key, mess) {
	document.getElementById(key + '_error').innerHTML = mess;
}

function validate() {
	var flag = true;

	var receiver = getValue('receiver');
	if (receiver == '') {
		flag = false;
		showError('receiver', 'Vui lòng nhập tên người nhận');
	}

	// email
	var email = getValue('email');
	var mailformat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if (!mailformat.test(email)){
		flag = false;
		showError('email', 'Email sai định dạng');
	}else if (email == '') {
		flag = false;
		showError('email', 'Vui lòng nhập tên email');
	}
	
	// phone
	var phone = getValue('phone');
	var phoneformat = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;
	if (!phoneformat.test(phone)){
		flag = false;
		showError('phone', 'Phone sai định dạng');
	}else if (phone == "") {
		flag = false;
		showError('phone', 'Vui lòng nhập số điện thoại');
	}
	
	return flag;

}