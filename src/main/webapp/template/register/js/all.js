function getValue(id) {
	return document.getElementById(id).value.trim();
}
function showError(key, mess) {
	document.getElementById(key + '_error').innerHTML = mess;
}

function validate() {
	var flag = true;
	// 1 username
	var username = getValue('username');
	if (username == '') {
		flag = false;
		showError('username', 'Vui lòng nhập tài khoản');
	}

	else if (username.length <= 5) {
		flag = false;
		showError('username', 'Tài khoản phải lớn hơn 5 ký tự');
	}

	else if (!/^([a-zA-Z0-9])+$/.test(username)) {
		flag = false;
		showError('username', 'Tài khoản không đúng định dạng');
	} else {
		showError('username', '');
	}

	// password
	var password = getValue('password');
	var repassword = getValue('repassword');

	if (password == '') {
		flag = false;
		showError('password', 'Vui lòng nhập mật khẩu');
	}

	else if (password.length < 6) {
		flag = false;
		showError('password', 'Mật khẩu phải lớn hơn 6 ký tự ');
	}

	else if (password != repassword) {
		flag = false;
		showError('password', 'Nhập lại mật khẩu không khớp');
	} else {
		showError('password', '');
	}
	return flag;

}